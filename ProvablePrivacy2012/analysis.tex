\section{Privacy and performance analysis}
\label{sec:analysis}
In this section, we analytically study the privacy offered by PANACEA by employing probability theory and information theory approaches. Moreover, we estimate the expected communication overhead of our approach.
\subsection{Adversary models}
\label{sec:adversarymodels}
In order to quantify the privacy properties of the system, first, we highlight on the various possible adversary models that are relevant to the PANACEA system. In a \textit{single} adversary model, a single adversary tries to breach the privacy of the system on his own without seeking support from others, which is normally the case in a \textit{collusive group} of adversaries model. In both cases, we study the case of privacy breach possible for \textit{non-interactive} and \textit{interactive} query models, where in the former the privacy attack involves a \textit{single} query from the user where as in the latter, \textit{multiple} queries.
\par
In addition, an adversary can assume multiple roles in the system: as a \textit{participant} in the randomized routing, a \textit{host} node where an index entry is hosted in the DHT, and a \textit{searcher}. An attacker typically can play some or all of these roles at the same time. 
\par
Unless specified otherwise, in all cases, we assume \textit{minimality attack} scenario \cite{minimality}. The minimality attack model assumes an attacker being well versed with technical details of the underlying system- namely for the case of PANACEA, the attacker is assumed to know the anonymization process, the values of $m$, $n$, and $\mu$. For example, the attacker can be part of the system and thus is aware of the parameters configured by the system administrators.
\par
We also discuss briefly a special type of adversary in the form of an authorized user who has access to a particular resource with a particular provider of an \textit{(m,n)-}entry. Similar to most of privacy-preserving data sharing systems, PANACEA's provider privacy can be breached by a a global adversary who can eavesdrop all the communication channels. However, we believe that resource privacy can not be breached. Further treatment of this adversary is not considered for brevity reasons.
\par
To begin with, we consider the case of single adversary and non-interactive query model. Later, we discuss other attack scenarios based on the formulations we make for this case.
\par
We use the following notation in our analysis. Let $N$ be the number of peers in the system and $N_c$ be the expected number of copies of a genuine resource $r$ and $N_a\le N_c$ be the number of copies that a particular user is authorized to access. We call a user as \textit{unauthorized} to $r$, when he is not authorized to access any of the $N_c$ copies of $r$, i.e. $N_a=0$. 
\subsection{Single searcher-single query adversary model}
We do the privacy analysis using two approaches: \textit{probabilistic approach} studies the privacy properties on a probabilistc scale and \textit{information theoretic approach} studies the effectiveness of anonymization technqiues in terms of entropy.
\subsubsection{Probabilistic approach} 
We evaluate the privacy breach possible by a user querying for a single resource in the minimiality scenario. %\textbf{Other cases where users have limited or no knowledge of the systems and case of attacks employing multiple queries to violate privacy of PANACEA, are discussed in Section \ref{sec:attacks}.}
We denote:
\begin{itemize} 
\item[i)] $P_{K,a}$ (resp. $P_{K,u}$) as the probability for an authorized (resp. unauthorized) user to deduce the existence of a certain genuine resource.
\item[ii)] $P_{V,a}$ (resp. $P_{V,u}$) as the probability for an authorized (resp. unauthorized) user to deduce the provider of a certain resource. 
\item[iii)] $P_{-}$ as the probability for an authorized or unauthorized user to deduce the non-existence of a certain {\em non-existing} resource.
\end{itemize}
\begin{definition}
An access-controlled system is said to provide \textit{higher} privacy if it promises: 
\begin{itemize}
\item[i)] Lower probability for an unauthorized user to deduce a resource's presence and its provider ($P_{K,u}$, $P_{V,u}$)
\item[ii)] Lower probability for a user deducing a resource's non-existence ($P_{-}$)
\end{itemize}
\label{def}
\end{definition}
Under this definition of privacy, any \textit{privacy-efficient} access control mechanism should aim to:
\begin{itemize}
\item Minimize $P_{K,u}$, $P_{V,u}$, $P_{-}$, which should ideally be 0 as in unstructured P2P systems.
\item Maximize search cost $C_{s,u}$ for unauthorized users and ideally close to that of the unstructured P2P systems.
\end{itemize}
However, the \textit{search efficiency} of the privacy-enabling mechanism should remain high, i.e.:
\begin{itemize}
\item $P_{K,a}$, $P_{V,a}$ should ideally be 1 (as in structured P2P systems), and
\item The search communication cost $C_{s,a}$ should be kept low and ideally close to that of the structured P2P systems.
\end{itemize} 
We express the privacy and search cost metrics of PANACEA in terms of the corresponding metrics of structured and unstructured P2P systems. To this end, we use superscripts $U$ and $S$ to denote metrics for unstructured and structured P2P systems respectively, and no superscript for PANACEA, e.g. $P_{K,u}^{U}$ refers to unstructured systems and the equivalent metric for PANACEA is $P_{K,u}$. 
\par
The analyis can be understood as a tool to help the system designer to analyze the posteriori probabilities achievable by unauthorized users possible in a minimality model of privacy breach, for various values of system parameters. %When we talk about resource privacy for example, based on above parameters esp Nc, Na, how the probability of inference is influenced- can be figured out. Since the formulation has Nc, it does not mean that attacker knows Nc.
\\
\underline{Computation of $P_{V,a}$:}
First, we quantify provider privacy for an authorized user. There are three cases that can arise:

\textit{Case (i):} If any of the $N_a$ copies, where he is authorized to, was published to the DHT, he could deduce the provider of the resource with probability $1$. The probability that at least one of $N_a$ copies was published into the DHT is $1-(1-\mu)^{N_a}$.

\textit{Case (ii):} On the other hand, consider the case that none of the $N_a$ copies was published into the DHT (probability of which, is $(1-\mu)^{N_a}$), but at least one of the remaining $N_c-N_a$ copies was published (probability of which is $(1-(1-\mu)^{N_c-N_a})$). In this case,  the user first contacts all the providers associated with $H(r)$ and then floods the search request, where he deduces the provider with probability $P_{V,a}^U$. In case of unsuccessful flooding, the user tries to deduce the provider from the provider list present in the DHT. For time being, lets assume the case the user already established the existence of the resource. If $l$ is the provider list size, it implies $\frac{l}{n}$ number of $(m,n)$- entries contributed to this resource's DHT entry. Hence, in this case, we claim that the provider is deduced with probability $\frac{l/n}{l}$ \footnote{after considering the probability of a valid provider being chosen randomly by another valid provider as part of publishing as negligible.}.
\par 
Now, lets assume the general case where the resource existence is not known to the user. In this case, in addition to finding a provider, the user has to decide on the genuinity of the resource. For one \textit{(m,n)-}entry, this can be understood as picking one \textit{(1,1)-}entry out of $m\cdot n$ number of \textit{(1,1)-} entries. Hence, for a provider list of size $l$, we have to choose $\frac{l}{n}$ number of \textit{(1,1)}-entries from a total of $l\cdot m$ number of entries, thus resulting in a provider privacy of $\frac{\frac{l}{n}}{l\cdot m}=\frac{1}{m\cdot n}$. 
\par
However, the provider list size need not grow in multiples of $n$ for each insertion, because of ``collisions'' (i.e. provider id conflicts) in the provider lists across the multiple $(m,n)$-entries of the resource copies. We account for this effect with a \textit{collision probability} $f_v$. 

\textit{Case (iii):} When a DHT entry is not found for the resource key (probability of which is $(1-\mu)^{N_c}$), the user attempts to deduce the provider by flooding ($P_{V,a}^U$). Hence,

\begin{equation}
\small
\begin{split}
P_{V,a}= &\left[1-(1-\mu)^{N_a}\right] \cdot 1  + (1-\mu)^{N_a}\cdot (1-(1-\mu)^{N_c-N_a}) \cdot \\
&\cdot\left[ P_{V,a}^U\cdot 1 + (1- P_{V,a}^U)\cdot\frac{1}{m\cdot n(1-f_v)}\right]\\
& +(1-\mu)^{N_c}\cdot P_{V,a}^U 
\end{split}
\label{pva_panacea}
\normalsize
\end{equation}
\underline{Computation of $P_{K,a}$:}
We apply similar reasoning to formulate resource privacy $P_{K,a}$ for an authorized user with the following consideration for case (ii): In case of unsuccessful flooding, the user tries to deduce the resource existence from the DHT entry. Here, the user can assign only a probability of $\frac{1}{m}$ to the existence, as the queried key is mixed with $m-1$ other ones in the $(m,n)$-entry, in addition to what can also be deduced by flooding ($P_{K,a}^U$). Therefore,
\begin{equation}
\small
\begin{split}
P_{K,a} = & \left[1-(1-\mu)^{N_a}\right] \cdot 1  + (1-\mu)^{N_a} \cdot (1-(1-\mu)^{N_c-N_a})\cdot \\
& \left[P_{K,a}^U \cdot1 + (1-P_{K,a}^U)\cdot \frac{1}{m}\right] +(1-\mu)^{N_c}\cdot P_{K,a}^U
\end{split}
\label{pka_panacea}
\normalsize
\end{equation}
\underline{Computation of $C_{s,a}$:} Next, we quantify the search cost $C_{s,a}$ in terms of the number of nodes visited by the search query from an authorized user. First, a user searches in the DHT which incurs a cost of $C_{s,a}^S$. Thereafter, we account for two possible cases- none of $N_c$ copies or some $i$ copies of the resource are published into the DHT. The former case can happen with probability $(1-\mu)^{N_c}$ where the user employs flooding, incurring a cost of $C_{s,a}^U$. In the latter case, $i\cdot n$ number of providers are contacted. If none of them has an authorized copy (probability of which, is $(1-\frac{N_a}{N_c})^{i}$), the user employs flooding. Overall, $C_{s,a}$:
\begin{equation}
\small
\begin{split}
% C_{p,a} & = \sum_{i=1}^{N_c}{{N_c}\choose{i}}\mu^i(1-\mu)^{(N_c-i)}\cdot i\cdot \left(\frac{1}{\lambda}+ m \cdot C_{p,a}^S\right) \\
C_{s,a} & = C_{s,a}^S +(1-\mu)^{N_c}\cdot C_{s,a}^U+ \sum_{i=1}^{N_c}{{N_c}\choose{i}}\mu^i(1-\mu)^{(N_c-i)}\cdot\\
&\left[i\cdot n\cdot(1-f_v)(1+f_k)+ \left(1-\frac{N_a}{N_c}\right)^{i} C_{s,a}^U\right]
\end{split}
\label{csa_panacea}
\normalsize
\end{equation}
Equations for $P_{K,u}$, $P_{V,u}$, and $C_{s,u}$ can be derived from eq. (\ref{pva_panacea}) to eq. (\ref{csa_panacea}) by having $N_a=0$ and replacing the terms $P_{V,a}^U$, $P_{K,a}^U$, $C_{s,a}^S$, $C_{s,a}^U$ by $P_{V,u}^U$, $P_{K,u}^U$, $C_{s,u}^S$, $C_{s,u}^U$ respectively.
\\
\underline{Computation of $P_{-}$ and $C_{s,-}$:} Finally, we derive $P_{-}$, i.e. the probability to deduce the non-existence of a non-existing resource. Given an event space $\Omega=\{$DHT,$\neg$DHT$\}$ that a non-existent resource is found or not in the DHT respectively, $P_{-}$ is given by:
\begin{equation}
\small
\begin{split}
P_{-} = Pr(-\mid\Omega) =  & Pr(-\mid \neg \textrm{DHT})\cdot Pr(\neg \textrm{DHT})+\\
& Pr(-\mid \textrm{DHT})\cdot Pr(\textrm{DHT})\>,\textrm{where}\\
%Pr[\textrm{Not Find}]& =  1-\left[\mu\cdot\frac{1}{m}+(1-\mu)P_{K,u}^U\right] \\
Pr(-\mid \neg \textrm{DHT}) =  & P_{K,u}^U = 0\\
Pr(-\mid \textrm{DHT}) = & \frac{m-1}{m} \\
Pr(\textrm{DHT}) = & \left[1-\left(1-\frac{1}{\mid R\mid}\right)^{\mu N_r (m-1)}\right] \\
Pr(\neg \textrm{DHT}) = & 1 - Pr(\textrm{DHT})
\end{split}
\label{pminus_panacea}
\normalsize
\end{equation}

$N_r$ is the total number of resources in the system and $R$ is the resource namespace. $Pr(-\mid \neg \textrm{DHT})$ expresses the probability that a resource is non-existent, given that it is not found in the DHT. This is similar to the probability of deducing the existence of an unauthorized resource for a user in unstructured P2P systems, because an existing resource is same as a non-existing resource for an unauthorized user. $Pr(-\mid \textrm{DHT})$ is the probability that the resource corresponding to the key does not exist. $Pr(\textrm{DHT})$ expresses the probability that a phantom resource from namespace $R$ may have been inserted into the index, while $Pr(\neg \textrm{DHT})$ is the complement of $Pr(\textrm{DHT})$. Observe that $P_{-}$ is minimal ($\sim 0$) for reasonable values of the various parameters. Also, we estimate the expected query cost to deduce the non-existence. The user first searches for an index entry and then employs flooding, hence,
\begin{equation}
\small
\begin{split}
C_{s,-} = C_{s,-}^S + C_{s,-}^U\>.
\end{split}
\label{csminus_panacea}
\normalsize
\end{equation}
\subsubsection{Information-theoretic approach}
\label{sec:entropy}
The privacy offered by systems employing anonymization can be quantified in terms of the effectiveness of the anonymization mechanisms, which is measured using information theoretic approach. In \cite{serjantov02towards,Diaz02towardsmeasuring}, such an approach was proposed to measure privacy   employing {\em entropy} $H$ as an anonymity metric, which is defined as: 
\begin{equation}
\small
\begin{split}
H=-\displaystyle \sum_i p_i \log_2 p_i\>,
\end{split}
\label{}
\normalsize
\end{equation}
 where $p_i$ is the attacker's estimate of the probability that a participant $i$ was responsible for some observed action. Entropy is maximized to $\log_2 |A|$ if equal probability is assigned to all members of the anonymity set $A$, and it is minimized to 0 when $|A|=1$. According to \cite{serjantov02towards}, a system with entropy $H$ has {\em effective anonymity set} of size $2^H$.
\par
As an adversary in PANACEA may have different information sets (i.e. each resulting from different observations), {\em conditional entropy} $H_0$ \cite{Diaz02towardsmeasuring} is a more appropriate metric, which is given by:
\begin{equation}
\small
\begin{split}
H_0=\displaystyle \sum_{y}Pr[Y=y]H(X|Y=y)=\sum_{y}E_y H(X|Y=y),
\end{split}
\label{}
\normalsize
\end{equation}
where $X$ is a random variable of the private aspect to be preserved and $Y$ models the different observations $y$. $E_y$ denotes expectation with respect to observation $y$. 
\par
In this section, we study the privacy properties of the system by introducing two metrics for a resource $r$ for the case of authorized searcher:
\begin{itemize}
\item $H_{K,a}$: resource entropy
 \item $H_{V,a}$: provider entropy 
\end{itemize}
\underline{Computation of $H_{V,a}$:} First, we calculate the entropy of PANACEA for provider anonymity against an authorized searcher. Here, the random variable $X$ models the publisher of the requested resource and the random variable $Y$ models possible information sets observed by the searcher:
(i) an authorized copy is found in the DHT, (ii) an unauthorized copy is found in the DHT and an authorized copy is found by flooding, (iii) an unauthorized copy is found in the DHT but no authorized copy is found by flooding, (iv) no copy is found in the DHT but an authorized copy was found by flooding, and (v) no copy is found in the DHT and no authorized copy was found by flooding.
If the searcher has made the observations (i), (ii) or (iv), then the provider entropy is 0. In case of observation (iii), where an unauthorized copy of the resource is found in the DHT and no authorized copy was found by flooding, we calculate $H(X|Y=iii)$ according to the following logic: $\mu(N_c-N_a)$ copies are expected to be published in the DHT resulting in a provider list of size $l=\mu (N_c-N_a)n(1-f_v)$. Therefore, the probability that a copy out of the $\mu (N_c-N_a)$ ones resides at one of these providers conditioned on the probability of resource's existence is $\frac{\mu (N_c-N_a)}{m\cdot l}=\frac{1}{mn (1-f_v)}$. Also, $(1-\mu)(N_c-N_a)$ copies are not published in the DHT and the probability that a copy resides at any other provider (i.e. apart from the $l$ ones) is $\frac{(1-\mu) (N_c-N_a)}{N-l}$. However, since there exist $N_c-N_a$ copies in total in this case, the sizes of the aforementioned sets of $l$ and $N-l$ number of providers are divided by $N_c-N_a$ to derive the effective anonymity set sizes. Finally, in the case of observation (v), where no copy is found in the DHT and no authorized copy was found by flooding, each peer in the set of $N$ peers has a probability of $\frac{N_c}{N}$ for being a provider. Therefore, the provider entropy $H_{V,a}$, is given by:
\begin{equation}
\small
\begin{split}
H_{V,a} =  & E_{\textrm{iii}}H(X|Y=\textrm{iii})+E_{\textrm{v}}H(X|Y=\textrm{v}) \\
= & -(1-\mu)^{N_a}(1-(1-\mu)^{N_c-N_a})(1-P_{V,a}^{U})\cdot \\
& \Big{[}\frac{l}{N_c-N_a}\frac{1}{mn (1-f_v)}\\
& \log\left(\frac{1}{mn (1-f_v)}\right)+\\
& \frac{N-l}{N_c-N_a}\frac{(1-\mu)(N_c-N_a)}{N-l}\log\left(\frac{(1-\mu)(N_c-N_a)}{N-l}\right)\Big{]} \\
& -(1-\mu)^{N_c}(1-P_{V,a}^{U})\left(\frac{N}{N_c}\right)\left(\frac{N_c}{N}\right)\log\left(\frac{N_c}{N}\right)
\end{split}
\label{p_entropy}
\normalsize
\end{equation}
\\
\underline{Computation of $H_{K,a}$:} Next, we calculate the system entropy for resource anonymity. To this end, the random variable $X$ models the existence of a resource, i.e. whether a resource name from the resource namespace $R$ exists in the system or not. The random variable $Y$ models the observations of the searcher for a requested resource as in the case of the provider entropy. The analysis follows a similar reasoning to the case of the provider entropy. Also, $l'=\mu(N_c-N_a)m$ is the expected number of keys in the DHT when $N_c-N_a$ copies may be published. Overall, the resource entropy $H_{K,a}$ is given by:
\begin{equation}
\small 
\begin{split}
H_{K,a} = & E_{\textrm{iii}}H(X|Y=\textrm{iii})+E_{\textrm{v}}H(X|Y=\textrm{v}) \\
%& -(1-(1-\mu)^{N_a}) 1 \log(1) \\
= &-(1-\mu)^{N_a}(1-(1-\mu)^{N_c-N_a})(1-P_{K,a}^{U})\cdot \\
&\Big{[}\frac{l'}{N_c-N_a}\frac{1}{m}\log(\frac{1}{m})+\frac{\mid R\mid-l'}{N_c-N_a}\cdot \\
&\frac{(1-\mu)(N_c-N_a)}{\mid R\mid-l'}\log\left(\frac{(1-\mu)(N_c-N_a)}{\mid R\mid-l'}\right)\Big{]}\\
& -(1-\mu)^{N_c}
%& (P_{K,a}^{U}\log(1)+
(1-P_{K,a}^{U})\log\left(\frac{N_c}{\mid R\mid}\right)\> 
\end{split}
\label{r_entropy}
\normalsize
\end{equation}
% % 
% \begin{equation}
% \begin{split}
% H_{K,a} = & E_{\textrm{iii}}H(X|Y=\textrm{iii})+E_{\textrm{v}}H(X|Y=\textrm{v}) = \\
% H_{K,a} = & E_{\textrm{iii}}H(X|Y=\textrm{iii})+E_{\textrm{v}}H(X|Y=\textrm{v})  \\
% %& -(1-(1-\mu)^{N_a}) 1 \log(1) \\
% % (P_{K,a}^{U}\log(1)+
% %) \\
% & -(1-\mu)^{N_c}
% %& (P_{K,a}^{U}\log(1)+
% (1-P_{K,a}^{U})\log(\frac{1}{R})\>,\\
% \textrm{ where} & \\
% \mathbf{I}'(l)& = \left\{\begin{array}{cl} \log(1) \textrm{,} ~~~\textrm{for } l > n_f \\ 
% \mu \log(\frac{1}{m})+(1-\mu)\log(\frac{1}{R}) \textrm{, otherwise} \\ \end{array} \right\}
% \end{split}
% \label{r_entropy}
% \end{equation}
%$E(n)=\mu(N_c-N_a)n(1-f_v)$ is the expected provider list size in the $(m,n)$-entry for the queried resource.


% \begin{equation}
% \begin{split}
% H_{V,a} &=  E_{\textrm{iii}}H(X|Y=\textrm{iii})+E_{\textrm{v}}H(X|Y=\textrm{v}) \\
%  &= -(1-\mu)^{N_a}(1-P_{V,a}^U)\cdot \\
% & \sum_{i=1}^{N_c-N_a}\Big{[}{{N_c-N_a}\choose{i}}\mu^i (1-\mu)^{(N_c-N_a-i)}\cdot \\
%  &\Big{(}\log_2{\frac{1}{\frac{in(1-f_v)}{i}}}+\log_2{\frac{1}{\frac{N-in(1-f_v)}{N_c-i}}}\Big{)}\Big{]}\\
% &-(1-\mu)^{N_c}(1-P_{V,a}^U)\log_2{\frac{1}{\frac{N}{N_c}}}
% %\mathbf{I'}(l)& = \left\{\begin{array}{cl} 1\textrm{,} ~~~\textrm{for } l> n_f \\ \frac{1}{m} \textrm{, otherwise} \\ \end{array} \right\}\>.
% \end{split}
% \label{p_entropy}
% \end{equation}
%the following is conditional entropy conditioned on resource existence
% \begin{equation}
% \begin{split}
% H_{V,a} = & E_{\textrm{iii}}H(X|Y=\textrm{iii})+E_{\textrm{v}}H(X|Y=\textrm{v}) \\
% & = -(1-\mu)^{N_a}(1-P_{K,a}^U)\cdot \\
% &\sum_{i=1}^{N_c-N_a}\Big{[}{{N_c-N_a}\choose{i}}\mu^i (1-\mu)^{(N_c-N_a-i)}\cdot\\
%  &\mathbf{I'}(i\cdot n(1-f_v))\cdot \Big{(}\log_2{\frac{1}{\frac{in(1-f_v)}{i}}}+\log_2{\frac{1}{\frac{N-in(1-f_v)}{N_c-i}}}\Big{)}\Big{]}\\
% &-(1-\mu)^{N_c}(1-P_{K,a}^U)\log_2{\frac{1}{\frac{N}{N_c}}}\textrm{, where}\\
% \mathbf{I'}(l)& = \left\{\begin{array}{cl} 1\textrm{,} ~~~\textrm{for } l> n_f \\ \frac{1}{m} \textrm{, otherwise} \\ \end{array} \right\}\>.
% \end{split}
% \label{p_entropy}
% \end{equation}

% OLD FORMULA USED IN ITC SUBMISSION
% \begin{equation}
% \begin{split}
% H_{V,a} = & E_{\textrm{iii}}H(X|Y=\textrm{iii})+E_{\textrm{v}}H(X|Y=\textrm{v}) \\
% = & -(1-\mu)^{N_a}(1-(1-\mu)^{N_c-N_a})(1-P_{K,a}^U)\\ 
% & (E(n')\frac{1}{E(n')+1}\log_2{\frac{1}{E(n')+1}}+\\
% & (N-E(n'))\frac{1}{(N-E(n'))(E(n')+1)}\\
% & \log_2{\frac{1}{(N-E(n'))(E(n')+1)}}\\
% & -(1-\mu)^{N_c}(1-P_{K,a}^U)\log_2{\frac{1}{N}}
% \end{split}
% \label{p_entropy}
% \end{equation}

The equations for $H_{V,u}$ and $H_{K,u}$ can be derived from eq. (\ref{p_entropy}) and (\ref{r_entropy}), respectively by replacing  $N_a=0$ and $P_{K,a}^U$, $P_{V,a}^U$ with $P_{K,u}^U$, $P_{V,u}^U$ respectively.
\subsubsection{The case of resource anonymization with Random Selection} 
The PANACEA system's privacy properties in the case of resource anonymization with random selection (RS) approach are analysed in this section. The RS-approach has a unique case w.r.t resource privacy which is explained in the following. Consider the case of a very popular resource which many providers in the system own and are willing to share through the PANACEA system. In RS-approach, as the resource keys are selected randomly as part of anonymization, the same phantom key being chosen by many providers is less likely compared to the key corresponding to the popular resource. Hence, by observing the sizes of the provider lists with keys in the DHT, one can assume that the keys with longer lists are more probable to be genuine than not. However, we believe that its not trivial for an adversary to estimate the maximum provider list size a phantom key can take in the system. Yet, we assume the worst case where the adversary can estimate this value, which is modeled by a parameter $n_f$ in the following analysis.  In the rest of the section, we denote the case of RS-approach with superscrpt \textit{RS}.
\\
\underline{Computation of $P_{K,a}^{RS}$:}
This forumaltion is done in a similar way it is done for eq. (\ref{pka_panacea}) in addition to the following. If the size of provider list of the $(m,n)$-entry of the queried resource is larger than $n_f$, the user can deduce the existence of the resource in the system with probability $1$. Otherwise, a probability $\frac{1}{m}$ can be assigned to the existence, as the queried key is mixed with $m-1$ other ones in the $(m,n)$-entry. We consider this observation for formulating the privacy properties.
\par
Moreover, note hat collisions in the resource keys are also possible as providers choose key randomly. These collision are accounted for, with a probability $f_k$, which increase the expected provider list size of a key \footnote{In fact, a DHT entry can also be present for the key being a phantom one. However, we assume this probability as negligible, as $|R|$ is big compared to the number of genuine resources.}.

\begin{equation}
\small
\begin{split}
P_{K,a}^{RS} = & \left[1-(1-\mu)^{N_a}\right] \cdot 1  + (1-\mu)^{N_a} \cdot (1-(1-\mu)^{N_c-N_a})\cdot \\
& \mathbf{I}(E(n)) +(1-\mu)^{N_c}\cdot P_{K,a}^U\textrm{, where}\\
\mathbf{I}(l)& = \left\{\begin{array}{cl} 1\textrm{,} ~~~\textrm{for } l > n_f \\ P_{K,a}^U \cdot1 + (1-P_{K,a}^U)\cdot \frac{1}{m} \textrm{, otherwise} \\ \end{array} \right.
\end{split}
\label{pka_panacea_rs}
\normalsize
\end{equation}
$E(n)=\mu(N_c-N_a)n(1-f_v)(1+f_k)$ is the expected provider list size for the queried resource.
\underline{Computation of $P_{V,a}^{RS}$:} The provider privacy for authorized user is computed as follows:
\begin{equation}
\small
\begin{split}
P_{V,a}^{RS}= &\left[1-(1-\mu)^{N_a}\right] \cdot 1  + (1-\mu)^{N_a}\cdot (1-(1-\mu)^{N_c-N_a}) \cdot \\
&\cdot\left[ P_{V,a}^U\cdot 1 + (1- P_{V,a}^U)\cdot\mathbf{I''(E(n))}\cdot\frac{1}{n(1-f_v)(1+f_k)}\right]\\
& +(1-\mu)^{N_c}\cdot P_{V,a}^U \textrm{, where}\>\\
\mathbf{I''}(l)& = \left\{\begin{array}{cl}1\textrm{,} \textrm{ for } l > n_f \\\frac{1}{m} \textrm{, otherwise} \\ \end{array} \right.
\end{split}
\label{pva_panacea_rs}
\normalsize
\end{equation}
For brevity, we skip the privacy analysis using the information theoretic approach.
% First, we calculate the entropy of PANACEA for provider anonymity against an authorized searcher. Here, the random variable $X$ models the publisher of the requested resource and the random variable $Y$ models possible information sets observed by the searcher:
% (i) an authorized copy is found in the DHT, (ii) an unauthorized copy is found in the DHT and an authorized copy is found by flooding, (iii) an unauthorized copy is found in the DHT but no authorized copy is found by flooding, (iv) no copy is found in the DHT but an authorized copy was found by flooding, and (v) no copy is found in the DHT and no authorized copy was found by flooding.
% If the searcher has made the observations (i), (ii) or (iv), then the provider entropy is 0. In case of observation (iii), where an unauthorized copy of the resource is found in the DHT and no authorized copy was found by flooding, we calculate $H(X|Y=iii)$ according to the following logic: $\mu(N_c-N_a)$ copies are expected to be published in the DHT resulting in a provider list of size $E(n)$. Therefore, the probability that a copy out of the $\mu (N_c-N_a)$ ones resides at one of these providers is $\frac{\mu (N_c-N_a)}{E(n)}=\frac{1}{n (1-f_v)(1+f_k)}$. Also, $(1-\mu)(N_c-N_a)$ copies are not published in the DHT and the probability that a copy resides at any other provider (i.e. apart from the $E(n)$ ones) is $\frac{(1-\mu) (N_c-N_a)}{N-E(n)}$. However, since there exist $N_c-N_a$ copies in total in this case, the sizes of the aforementioned sets of $E(n)$ and $N-E(n)$ number of providers are divided by $N_c-N_a$ to derive the effective anonymity set sizes. Finally, in the case of observation (v), where no copy is found in the DHT and no authorized copy was found by flooding, each peer in the set of $N$ peers has a probability of $\frac{N_c}{N}$ for being a provider. Therefore, the provider entropy $H_{V,a}$, is given by:
% \textit{/* fix it */}
% \begin{equation}
% \small
% \begin{split}
% H_{V,a} =  & E_{\textrm{iii}}H(X|Y=\textrm{iii})+E_{\textrm{v}}H(X|Y=\textrm{v}) \\
% = & -(1-\mu)^{N_a}(1-(1-\mu)^{N_c-N_a})(1-P_{V,a}^{U})\cdot \\
% & \Big{[}\frac{E(n)}{N_c-N_a}\mathbf{I''(E(n))}\frac{1}{n (1-f_v)(1+f_k)}\\
% & \log\left(\mathbf{I''(E(n))}\frac{1}{n (1-f_v)(1+f_k)}\right)+\\
% & \frac{N-E(n)}{N_c-N_a}\frac{(1-\mu)(N_c-N_a)}{N-E(n)}\log\left(\frac{(1-\mu)(N_c-N_a)}{N-E(n)}\right)\Big{]} \\
% & -(1-\mu)^{N_c}(1-P_{V,a}^{U})\left(\frac{N}{N_c}\right)\left(\frac{N_c}{N}\right)\log\left(\frac{N_c}{N}\right)
% \end{split}
% \label{p_entropy_rs}
% \normalsize
% \end{equation}
% 
% Next, we calculate the system entropy for resource anonymity. To this end, the random variable $X$ models the existence of a resource, i.e. whether a resource name from the resource namespace $R$ exists in the system or not. The random variable $Y$ models the observations of the searcher for a requested resource as in the case of the provider entropy. The analysis follows a similar reasoning to the case of the provider entropy. Note that in the information set (iii), if the expected size of the provider list of the $(m,n)$-entry is greater than $n_f$, then the resource is genuine and there is no anonymity. Also, $E(m)=\mu(N_c-N_a)m(1-f_k)$ is the expected number of keys in the DHT when $N_c-N_a$ copies may be published. Overall, the resource entropy $H_{K,a}$ is given by:
% \begin{equation}
% \small 
% \begin{split}
% H_{K,a} = & E_{\textrm{iii}}H(X|Y=\textrm{iii})+E_{\textrm{v}}H(X|Y=\textrm{v}) \\
% %& -(1-(1-\mu)^{N_a}) 1 \log(1) \\
% = &-(1-\mu)^{N_a}(1-(1-\mu)^{N_c-N_a})(1-P_{K,a}^{U})\cdot\mathbf{I}'(E(n)) \\
% & -(1-\mu)^{N_c}
% %& (P_{K,a}^{U}\log(1)+
% (1-P_{K,a}^{U})\log\left(\frac{N_c}{\mid R\mid}\right)\>,\textrm{ where} \\
% \mathbf{I}'(l) = & \left\{\begin{array}{l} \log(1) \textrm{, for } l > n_f \\ 
% \Big{[}\frac{E(m)}{N_c-N_a}\frac{1}{m (1-f_k)}\log(\frac{1}{m (1-f_k)})+\frac{\mid R\mid-E(m)}{N_c-N_a}\cdot \\
% \frac{(1-\mu)(N_c-N_a)}{\mid R\mid-E(m)}\log\left(\frac{(1-\mu)(N_c-N_a)}{\mid R\mid-E(m)}\right)\Big{]} \textrm{, otherwise}
%  \\ \end{array}\right. 
% \end{split}
% \label{r_entropy_rs}
% \normalsize
% \end{equation}