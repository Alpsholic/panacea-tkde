\section{Solution Mechanism}
To meet the design goals of HAC, we propose a privacy preserving indexing mechanism that considers resource and provider privacies. The HAC system uses a DHT overlay to host this index. To be seen later, HAC employs GAC- like flooding for certain resources to meet its goals. Privacy preserving index is often used for sharing access controlled resources in the network \cite{privacyPreserving}. Users search the index to reach providers who can enforce the access control policies on the resource access to stop the illegitimate users from accessing the resource. 
% In the rest of the draft, we assume systems where AC is enforced always at provider nodes, and thus the content privacy is never breached. For example, in an access control aware Gnutella, the resource provider can verify the authorization credentials of the requesting peer and enforce the access control before returning the resource in reply to a search query. Hence, the paper deals with resource and provider privacies. 
\par
The proposed indexing mechanism consists of tunable parameters. Such tunable knobs allow system designers to choose between strong privacy vs publishing and searching overhead. The system is highly configurable and the tuning determines the position of the resulting system on the privacy and performance graphs (Figure. \ref{comparison}), which will be explained more in the following. We refer to such resource and provider privacy preserving index as \textit{RPP index} in the rest of the draft.%The system can be tunable in such a way that the probabilities of leakage become smaller and smaller which can be ignored in practice, hence realizing a $L_2KV$ system with PRA indexing technique alone.
\subsection{Design of HAC}
HAC realizes the resource and provider privacies using two techniques:
\begin{enumerate}
 \item Probabilistic publishing
\item Resource and provider privacy preservation (RPP) indexing 
\end{enumerate}
We illustrate these in detail in the following.
\subsubsection{RPP index}
We resort to {\it k-anonymization} techniques to achieve both resource and provider privacies. A k-anonymization technique typically anonymizes a data item by hiding it inside a list of $k$ data items so that a user can not make out anything about the original intended data item. Instead of a $(key, value)$ pair forming an index entry for a resource as done in CAC- like systems, we propose to have a pair of list of keys and list of values $(key[], value[])$ forming the index entry. We refer to such an index entry as {\it (m,n)- index entry} where $m$ refers to size of the {\it key list} and $n$ refers to that of {\it value list}. Hence, an index entry of the conventional P2P systems can be called as {\it (1,1)- index entry}. In the following, we discuss how an {\it (m,n)- entry} can be constructed and inserted into the structured P2P system. Greater the values for $m$ and $n$, higher the privacy they offer but at increased publishing and searching overhead as to be explained later. A \textit{(1,1)} index entry represents the fact \textit{a particular peer publishes a particular resource}, where as a \textit{(m.n) entry} mentions \textit{one of these listed peers publishes one of the listed resources}.
\par
However, it should be observed that resource privacy has two aspects- one concerns with existing and shared resources, the second with non-existing resources: an unauthorized user should not be able to determine that a particular resource does not exist in the system, a property which is inherent to GAC. $(m,n)$ indexing is introduced to achieve the former aspect. To address the latter, we resort to probabilistic publishing described below. 
\subsubsection{Probabilistic publishing}
Instead of announcing every resource through index as done in CAC, a publisher in HAC, decides to announce a resource through an {\it (m,n)- index entry} with a system- defined probability $\mu$. This probabilistic publishing is introduced, as explained earlier, to hide the non-availability of a certain resource in the system. Hence, a user by inspecting the index, can not figure out this fact. Absence of a key in the index does not guarantee non-availability of the corresponding resource in the system.
\par
Because of probabilistic publishing, HAC turns into a hybrid system that that acts partly like GAC and partly like CAC. All the resources which are not announced in the index are discovered using GAC-like flooding. 
\par
Hence, the publishing mechanism in HAC involves majorly two steps.
\begin{enumerate}
 \item Determine probabilistically whether to make the resource available through index or not. If yes,
 \item Generate RPP index entry and insert into the network
\end{enumerate}
\subsection{Generating and inserting RPP index}
The process of generating and inserting RPP index entry for a resource applies mainly the following steps on the (1,1) index corresponding to a resource.
\begin{enumerate} 
 \item Anonymize the resource to achieve resource privacy
 \item Anonymize the provider to realize provider privacy
 \item Anonymously publish the resulting index entry into the DHT to hide the intended provider
\end{enumerate}
These steps are captured in Figure. \ref{pra}.
\begin{figure}[htb]
\centering
\includegraphics[height=3.5cm, width=8cm]{Figures/pra.eps}
\caption{RPP index generation process}
\label{pra}
\end{figure}
\par
The RPP index generation process is explained in detail in the following sub sections.
\subsubsection{Resource anonymization}
Once a resource is selected for publishing, an equivalent {\it (1,1) index} is input into the {\it Resource Anonymizer (RA)} as shown in the figure for anonymizing the resource which converts it into a \textit{(m,1) index}. We propose two kinds of such anonymization techniques which build the key list either \textit{randomly} or using \textit{semantically closer keys}. Here, we briefly outline both the techniques and compare based on the ease of potential inference attacks to infer the valid key from the list. 
\par
It should be noted that human-readable plain text keys (\textit{resource names}- often used by users to refer to resources) are hashed to generate the system generated $keys$ (\textit{resource ids}) in any P2P system. We refer to such a resource namespace as $\Re$ and the equivalent resource id  space as $\aleph$. The hash function used in the system maps a key from $\Re$ to some key of $\aleph$. 
\par
When a resource with resource name $r$ has to be published, the \textit{(1,1) entry} for resource id $\aleph(r)$ is anonymized as in one of the two following ways.
\begin{itemize}
 \item Randomized anonymization
\item Semantics based anonymization
\end{itemize}
In the \textit{randomized anonymization} technique, \textit{m-1} number of entries are chosen randomly from the set $\Re$ and corresponding maps from $\aleph$ make the \textit{m} entries needed for an \textit{(m,1) index}. 
\par
In \textit{semantics based anonymization}, a set $S \subset \Re$ ($\mid S\mid<<<\mid\Re\mid$) of semantically related to $r$ are selected and the corresponding mappings form the $m$ entries. We assume that the user feeds such semantically related resource names using appropriate interfaces and automatizing the task is a research topic in itself, which we undertake as a future research study. For a resource name \textit{Obama speech.avi}, some possible semantically related terms would be \textit{Gandhi speech.avi, Lincoln speech.avi, Bush video.avi}. 
\par
All these meaningful resource names are potential names for a resource in the system, hence inferring the valid resource name from the list would be difficult. On the other hand, in the randomized anonymization, since the resource names are often semantically unrelated, inferring the valid resource name would be less difficult. However, determining the $m$ resource names from a \textit{(m,1) index} involves determining the resource name from the resource ids, which is possible only by a brute-force approach involving a dictionary attack. Since $\mid S\mid<<<\mid\Re\mid$, such an attack in semantic based anonymization is computationally much easier than in randomized anonymization. 
\par
Hence, in the case of adversaries with powerful computing resources, semantic based anonymization coupled with carefully chosen inference-resistant semantically closer keys must be preferred. Addressing the inferencing attacks possible on such an index entry is beyond the scope of the paper.
\subsubsection{Provider anonymization}
After resource anonymization, the resulting \textit{(m,1) index entry} is fed into the provider anonymizer. The provider list is populated with $n$ number of entries with the peer itself being one in the list. Other $n-1$ entries are chosen randomly from the \textit{Provider Store (PStore)}. We assume that PStores on each peer are populated with some providers in the network during network bootstrapping phase. The PStores on peers expand incrementally over life time of the network. Each peer when sees other $put()$ requests traversing through it, will retrieve the provider list from the messages and adds the providers into the local PStore.
\subsubsection{Anonymous publishing}
After an $(m,n)$ index entry is constructed by a publishing peer, it will be inserted into the network using DHT's $put()$ interface. However, a put interface inserts only a \textit{(1,1)} index which is coupled with a $get$ completes a successful resource publishing and retrieval. We resort to the following mechanism to publish a \textit{(m,n)} index using the put- interface without breaking the above well-established paradigm.
\par
A $put(1,1)$ interface can be straightaway used for a $put(1,n)$ request, as the $value$ field is immaterial during the routing. We convert a $put(m,n)$ request into $m$ number of $put(1,n)$ requests in the following way. 
\par
One of the $m$ keys is selected as a \textit{pivot} key and a $put(1,n)$ request is generated as if this pivot is the only key and the remaining $m-1$ keys are passed with the message along with the value list. Once this request reaches the node responsible for this id as per the DHT, the $(m,n)$ entry is constructed. And for any $get$ request on this pivot key which this node receives, the entire $(m,n)$ index entry is passed to the searcher in the reply. Thus a $(m,n)$ index entry completes a successful publishing and retrieval. Thus the same (m,n) entry is replied for a search on any of the $m$ keys. Since every key is made as a pivot, there will be $m$ number of $put()$ requests for a $(m,n)$ entry. 
\par
However, the DHT's $put()$ operation reveals which of these $n$ providers is a valid provider, because the immediately following peer as part the put() operation can easily attribute the $m$ keys to the peer which is initiating the put() operation. To circumvent the problem, we propose a {\it randomized routing (RR)} phase which {\it precedes} the DHT put() phase which hides the publisher node. The RR phase is similar to the \textit{tunneling} concepts introduced in \cite{crowds} with few modifications- the nodes instead of passively forwarding, contribute to the value list actively. As part of RR, in each hop, a peer is randomly chosen and the \textit{(m,n)} entry is forwarded to with a finite probability $\kappa$ or the DHT's routing is \textit{initiated} with probability $1-\kappa$ i.e., $put(m,n)$ request is initiated. Thus in RR phase, each peer either {\it forwards} with probability $p$ (where the system continues to be in RR phase) or {\it enters} the DHT routing (put()) operation with probability $1-\kappa$. 
\par
The above mechanism ensures that valid provider from the provider list is not leaked during index insertion into the DHT. 
\begin{enumerate}
 \item When RR phase is initiated by the publishing peer for a new (m,n) entry, there is no way the following peer to determine whether the preceding peer is actually inserting a fresh entry or forwarding someone's \textit{(m,n)} entry.
\item Even when a peer decides to enter the DHT routing, it is obvious from the above mechanism that it is executing the put request on behalf of some one else. 
\end{enumerate}
Thus the routing mechanism overcomes the problem that would have existed in case the index entry was inserted with a direct put() operation.
\par
When such an index entry is passed from a peer $P_i$ to $P_j$, if $P_i$ is in the provider list it means either it itself is the publisher and hence appears in the list or some other publisher inserted it. To blur this distinction, we assume that when a peer forwards the message, it adds itself to the list with a certain limited probability\footnote{Hence, the final size of the list after RR phase will be slightly more than $n$. In our computations, we ignore this fact for brevity reasons.}. This allows to have repetition while choosing the random nodes, and at the same time, allows the node to add itself to the provider list in case one of the $m$ resources listed exist in the system. Similarly, if $P_j$ is already in the list when $P_i$ forwards to $P_j$, to blur the fact that it is the publisher who inserted $P_j$ in the list, a node when forwarding a message adds the following peer to the list with certain probability. Hence, $P_j$ can not differentiate between the publisher and $P_i$.
\par
The RR phase introduces additional routing overhead compared to DHT's put(), but this overhead is always bounded by a deterministic limit set by the probability $\kappa$. The RR phase always converges into the DHT routing after a finite number of hops as shown below. 
\par
 If X is a random variable representing the number of hops taken by the RR phase, then
\begin{eqnarray*}
  P(X = l) & = & \kappa^{l-1}(1-\kappa)
\end{eqnarray*}
 
\par
X follows binomial distribution and hence
 \begin{eqnarray*}
 E(X)& = &1/\kappa  
 \end{eqnarray*}

Hence, the proposed RR phase introduces additional $1/\kappa$ number of hops on average over the DHT routing's cost.
% \begin{enumerate}
% \item How to select $k$ peer ids for a k-index entry? The issues- how to get k- ids and what kind of ids to be selected? 
% \item how to select the random peer? any criterion? like dont select the one from the list?? Add the list of peers into local cache.
% \item fwd from $p_i$ and $p_i \in value[]$- what is the probability that $p_i$ is assumed to be bogus? SOLUTION: To the initial $k$- set, each fwding peer arbitrarily adds itself with a small probability. Thus a peer seeing such a msg can not know whether it is adding itself like this OR it is indeed generating the msg.
% \item If some peer which receives the msg finds itself in the value list, the anonymity is broken into $k-1$ instead of $k$.  Initially it appears that if the msg is passed through $k-1$ such nodes and all collude together to figure out the valid value out of k-values. But such a probability (TODO- compute this) is very low. And when $k > n$,  it is never a problem.
% \item We assume that nodes cooperate with the system- the probability $p$ is honored, otherwise, 
% \item A receiving peer knows the IDs in the list and can contact them to filter out invalid values. It can be an allowed limitation of the system. As long as at most k-2 nodes collude, there is no privacy leakage.
% \end{enumerate}
\subsection{Searching}
In this section, we describe how search works in the HAC system. When a peer searches for a resource with id $r$, it executes \textit{get(r)}. As per the DHT routing, this request ends up at peer responsible for the key $r$. If a $(m,n)$ index entry was published with $r$ being one of the $m$ ids, then the peer returns this index entry to the searcher. Then the searcher contacts all of the $n$ providers listed, with his credentials. We assume that a user does not know before hand, where he is authorized for a particular resource and where he is not. If it is known before hand, the user can choose to select a few nodes from $n$ and contact them for accessing the resource. Access to the resource is granted if authorized. If no $(m,n)$ entry exists, the searcher then floods the search request. The PStores used by the PA- subsystem are used for this purpose. The peer floods to some fixed number of peers chosen from its PStore. 
\par
Hence, HAC resorts to {\it selective flooding}- flood to a fixed $n$ number of peers, had a RPP index entry existed for the resource. Otherwise, it floods like in GAC. However, in case of multiple providers for same resource, because of probabilistic publishing, an (m,n) entry for an existing resource may not contain all the providers of the resource in the system as some providers may skip to publish the resource. Hence, the index entry is not \textit{complete} w.r.t covering all providers of the resource in the system. As a result, a searcher may not be able to reach a provider through the RPP index, where he has privileges to access the resource. So even though, an (m,n) entry is present in the index, the searcher may choose to flood in HAC so as to increase his possibility to reach a provider where he is authenticated. As we observe in our analysis, the probability of a search being queried onto both the DHT and the flooding is very less. For reasonable values of $\mu$, with high probability the index entries would be complete covering all potential providers of a resource.
\par
However, since $m$ entries in an index entry are chosen independently on peers, collisions are possible. However, this does not pose any problem w.r.t privacies in HAC. We take on a simple approach for such entries. Each (m,n) entry is seen as an independent entry from other (m,n) entries even though some the keys in both the key lists overlap. When a search for such an overlapping key is received, all the (m,n) entries where the key appears, are returned. It is left to the application to determine the interpretation of such a set of (m,n) entries, for example, the searcher can contact providers in all the index entries to access the resource. We assume in our analysis in Section \ref{analysis}, that the searcher contacts all providers listed in all the index entries.

