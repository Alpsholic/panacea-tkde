\section{Evaluation}
\label{sec:evaluation}
In this section, we numerically evaluate PANACEA as related to unstructured and structured access-controlled P2P systems based on the analysis of Section \ref{sec:analysis}. Unless otherwise specified, we assume that each system consists of $N=10000$ peers with the unstructured system organized in an overlay graph of average degree $d=4$. The searcher is assumed to be authorized to $N_a=5$ out of $N_c=50$ copies of the queried resource. The TTL employed for limited-hop flooding is $ttl=6$ and the parameters of PANACEA are: $m=5, n=5, \lambda=0.2, \mu=0.6$. The collision parameters are specified as $f_k=0.1$, $f_v=0.4$.
\par
Figure \ref{fig:PaK_mu} shows the effect of $\mu$ on the search efficiency for an authorized user. As the probability of publishing $\mu$ increases, PANACEA approaches the search efficiency of a structured system. It is interesting to observe that for only $0.05\%$ of authorized copies of the resource in the system, setting a small value of $\mu=0.6$ makes the search efficiency of PANACEA close to that of structured systems. Hence, we claim that $\mu=0.6$ is the {\em appropriate} value for this parameter for the given network and configuration. On the other hand, for unauthorized users, increasing $\mu$ makes the search efficiency of the proposed system close to that of unstructured P2P systems, as shown in Figure \ref{fig:PuK_mu}. Therefore, PANACEA design meets its privacy objectives of Section \ref{sec:analysis}. As $\mu$ increases, so is the number of RPP index entries, which reduces the probability of deducing the existence of the resource as the list of potential providers grows.
\begin{figure}
\begin{minipage}[b]{0.45\linewidth}
\centering
\includegraphics[height=3.8cm, width=4cm]{MathematicaScripts/Graphs/PaK_mu_conf1.pdf}
\caption{$P_{K,a}$ \textrm{vs} $\mu$}
\label{fig:PaK_mu}
\end{minipage}
\hspace{0.5cm}
\begin{minipage}[b]{0.45\linewidth}
\centering
\includegraphics[height=3.8cm, width=4cm]{MathematicaScripts/Graphs/PuK_mu_conf1.pdf}
\caption{$P_{K,u}$ vs $\mu$}
\label{fig:PuK_mu}
\end{minipage}
\end{figure}
\begin{figure}[ht]
\begin{minipage}[b]{0.45\linewidth}
\centering
\includegraphics[height=3.8cm, width=4cm]{MathematicaScripts/Graphs/PaK_m_conf1.pdf}
\caption{$P_{K,a}$ \textrm{vs} $m$}
\label{fig:PaK_m}
\end{minipage}
\begin{minipage}[b]{0.45\linewidth}
\centering
\includegraphics[height=3.8cm, width=4cm]{MathematicaScripts/Graphs/PuK_m_conf1.pdf}
\caption{$P_{K,u}$ vs $m$}
\label{fig:PuK_m}
\end{minipage}
\end{figure}
\begin{figure}
\begin{minipage}[b]{0.45\linewidth}
\centering
\includegraphics[height=3.8cm, width=4cm]{MathematicaScripts/Graphs/Csa_mu_conf1.pdf}
\caption{$C_{s,a}$ vs $\mu$}
\label{fig:Csa_mu}
\end{minipage}
\begin{minipage}[b]{0.45\linewidth}
\centering
\includegraphics[height=3.8cm, width=4cm]{MathematicaScripts/Graphs/Csu_mu_conf1.pdf}
\caption{$C_{s,u}$ vs $\mu$}
\label{fig:Csu_mu}
\end{minipage}
\end{figure}
\begin{figure}[ht]
\begin{minipage}[b]{0.45\linewidth}
\centering
\includegraphics[height=3.8cm, width=4cm]{MathematicaScripts/Graphs/Csa_n_conf1.pdf}
\caption{$C_{s,a}$ \textrm{vs} $n$}
\label{fig:Csa_n}
\end{minipage}
\begin{minipage}[b]{0.45\linewidth}
\centering
\includegraphics[height=3.8cm, width=4cm]{MathematicaScripts/Graphs/Csa_Nc_conf1.pdf}
\caption{$C_{s,a}$ \textrm{vs} $\frac{N_c}{N}$}
\label{fig:Csa_Nc}
\end{minipage}
\end{figure}
In Figures \ref{fig:PaK_m} and \ref{fig:PuK_m}, we highlight the effect of $m$ on resource privacy for authorized and unauthorized users respectively. Clearly, the value of $m$ has no effect on the search efficiency of authorized users for a given value of $\mu$, as they discover the resource as long as at least one provider with an authorized copy of the resource publishes in the DHT (as the provider is then directly contacted). However, for unauthorized users, as $m$ increases, their probability to deduce the existence of the resource decreases due to the increased number of fake resource keys in the index entry, which is shown in Figure \ref{fig:PuK_m}. Thus, with increase in $m$, PANACEA search efficiency reaches that of unstructured P2P systems for unauthorized users. Similar results were obtained for the provider privacy of authorized and unauthorized users, while varying $\mu$ and $m$. We omit these results for brevity reasons.
\par
Figure \ref{fig:Csa_mu} depicts the effect of $\mu$ on the search communication cost for authorized users. As $\mu$ increases, the probability to find a provider where the user is authorized also increases. Hence, the user finds the resource in the DHT, and thus need not employ flooding. After $\mu=0.6$, there is no more cost improvement. The almost constant difference between the PANACEA and structured systems costs is due to: i) the decreasing probability that the resource is not found in the DHT and then flooding has to be employed, and ii) the increasing number of providers that have to be contacted as $m$ grows and more index entries are published in the DHT (refer to (\ref{csa_panacea})). However, it should be observed from Figure \ref{fig:Csu_mu} that increase in $\mu$ proves to be more {\em costly} for unauthorized users, which is a highly desirable property of PANACEA. The search cost with respect to $n$ is depicted in Figure \ref{fig:Csa_n}. As $n$ increases, the search cost increases as the user has to contact more number of providers. However, observe that, even for $n=10$, the search cost of PANACEA is several times lower compared to that of unstructured systems. 

Clearly, Figures \ref{fig:PaK_mu} to \ref{fig:Csa_n} prove our initial claims on PANACEA positioning as related to the structured and unstructured P2P systems as illustrated in Figure \ref{fig:pg}. In Figure \ref{fig:Csa_Nc}, as the number of providers of the resource $N_c$ increases in the overlay, we observe a linear increase in the search cost of PANACEA. However, this cost remains significantly lower than that of the unstructured P2P systems, and it can be considered as highly tolerable given the privacy benefits of PANACEA. If, for certain application, this is undesirable in terms of scalability, it could be addressed by properly adjusting values of the parameters $\mu$ and $n$. The search cost can also be improved having a user contacting the providers listed in an RPP index sequentially instead of concurrently.
%In addition, a user can figure out the providers where he is more probable to be authorized than not, either from his past history or from the type of credentials he has and the providers need for authorization.
% this explains P- behaviour w.r.t old equations.
% As explained in section. \ref{sec_mechanism}, the probabilistic publishing was introduced to hide the non-existing resources is illustrated in Figure \ref{fig:Pminus_mu}. However, PANACEA's performance w.r.t $P_a^{-}$ is appreciable only for smaller values of $\mu$. For higher values of $\mu$, a non-presence of an a resource in the index can be attributed to non-existence of the resource in the system with high probability. However, in practice this is not an issue as the value of $N_c$ which determines the value of $P_a^{-}$ (from Equation. \ref{equn:PaMinus}) can not be trivially guessed by a user. And more over, in stead of a system specified value for $\mu$, if each provider chooses a random value for $\mu$, we can improve the performance of $P_a^{-}$ which we pursue as future study.
%
%
% this explains P- behavoir wrt updated formulae but even this was updated later. So removing it ..
% As explained in section. \ref{sec_mechanism}, the probabilistic publishing was introduced to hide the non-existence of resources. As illustrated in Figure \ref{fig:Pminus_mu}, the PANACEA system design justify its goals in this case also.

Finally, as explained in Section \ref{sec:approach}, probabilistic publishing was introduced to hide the non-existence of resources. As shown in equations (\ref{pminus_u}) and (\ref{pminus_panacea}), the PANACEA system meets its design objectives (Section \ref{sec:analysis}) in this case as well, as $P_{-} \sim P_{-}^{U}$. Moreover, as shown in equations (\ref{csminus_panacea}), $C_{s,-} \sim C_{s,-}^U$. We omit the relevant graphs for brevity reasons.
% \begin{figure*}[ht]
% \begin{minipage}[b]{0.33\linewidth}
% \centering
% %\begin{table}
% \begin{tabular}{ |l | c | }
% \hline			
%   $N_a$ & 5  \\
%   $N$ & 10000  \\
%   $N_c$ & 50  \\
% $m$ & 5  \\
% $n$ & 5  \\
% $d$ & 4  \\
% $ttl$ & 6  \\
% $\lambda$ & 0.2  \\
% $\mu$ & 0.6  \\
% \hline  
% \end{tabular}
% \caption{List of values for various parameters}
% \label{table:values}
% %\end{table}
% \end{minipage}
% \begin{minipage}[b]{0.33\linewidth}
% \centering
% \includegraphics[height=4cm, width=5cm]{MathematicaScripts/Graphs/PaK_mu_conf1.pdf}
% \caption{$P_a^K$ \textrm{vs} $\mu$}
% \label{fig:PaK_mu}
% \end{minipage}
% \begin{minipage}[b]{0.33\linewidth}
% \centering
% \includegraphics[height=4cm, width=5cm]{MathematicaScripts/Graphs/PuK_mu_conf1.pdf}
% \caption{$P_u^K$ vs $\mu$}
% \label{fig:PuK_mu}
% \end{minipage}
% \end{figure*}
% \begin{figure*}[ht]
% \begin{minipage}[b]{0.33\linewidth}
% \centering
% \includegraphics[height=4cm, width=5cm]{MathematicaScripts/Graphs/PaK_m_conf1.pdf}
% \caption{$P_a^K$ \textrm{vs} $m$}
% \label{fig:PaK_m}
% \end{minipage}
% \begin{minipage}[b]{0.33\linewidth}
% \centering
% \includegraphics[height=4cm, width=5cm]{MathematicaScripts/Graphs/PuK_m_conf1.pdf}
% \caption{$P_u^K$ vs $m$}
% \label{fig:PuK_m}
% \end{minipage}
% \begin{minipage}[b]{0.33\linewidth}
% \centering
% \includegraphics[height=4cm, width=5cm]{MathematicaScripts/Graphs/Csa_mu_conf1.pdf}
% \caption{$C_s$ for authorized vs $\mu$}
% \label{fig:Csa_mu}
% \end{minipage}
% \end{figure*}
% \begin{figure*}[ht]
% \begin{minipage}[b]{0.33\linewidth}
% \centering
% \includegraphics[height=4cm, width=5cm]{MathematicaScripts/Graphs/Csu_mu_conf1.pdf}
% \caption{$C_s$ for unauthorized vs $\mu$}
% \label{fig:Csu_mu}
% \end{minipage}
% \begin{minipage}[b]{0.33\linewidth}
% \centering
% \includegraphics[height=4cm, width=5cm]{MathematicaScripts/Graphs/Csa_n_conf1.pdf}
% \caption{$C_s$ for authorized \textrm{vs} $n$}
% \label{fig:Csa_n}
% \end{minipage}
% \begin{minipage}[b]{0.33\linewidth}
% \centering
% \includegraphics[height=4cm, width=5cm]{MathematicaScripts/Graphs/Csa_Nc_conf1.pdf}
% \caption{$C_s$ for authorized \textrm{vs} $N_c/N$}
% \label{fig:Csa_Nc}
% \end{minipage}
%\end{figure*}