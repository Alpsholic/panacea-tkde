\section{Introduction}
Access controlled data sharing systems require a trusted way of enforcing access control function on the access requests. Quite often, it is infeasible to find a node mutually trusted by all publishers in the system to enforce access control on their behalf. Hence, ideally, such a trusted node would be the publishing node itself enforcing access control on the resources it shares in the system. Peer-to-Peer overlay networks connecting all the publishers and users emerge as the viable solution for realizing such a system. In a trivial setup, a Gnutella network \cite{gnutella} can be augmented with access control (referred as Gnutella with Access Control- \textit{GAC}), a searching node/user can reach the publisher (or provider) of a particular resource using flooding routing. Once the corresponding publisher is reached, the searcher can provide his credentials as defined by the authorization policies by the provider node, which are verified before the query is replied. 
\par
However, the performance of access controlled P2P systems is a critical requirement for wider adaptability. Typically it can be quantified as {\it search} and {\it publication} cost together measured in terms of the network cost (number of messages exchanged) incurred during search and publication respectively. The performance of GAC-like systems is an issue of concern as it involves expensive flooding routing techniques. To improve the performance, the most adopted mechanism is to reduce the search cost significantly by building an index of the resources shared in the system and hosting the index distributedly over the network, for e.g., in the form of DHTs. Such an index is normally in the form of \textit{(key, value)} pairs where $key$ points to unique resource identifier and $value$ points to the provider identifier\footnote{We denote them using {\it K} and {\it V} respectively}. For example, an access controlled DHT-based system could be realized on \textit{Chord}- (CAC)\footnote{We refer any index-free AC P2P system with GAC and index-based P2P system with CAC. We chose Gnutella and Chord for illustration purpose.}, where chord overlay is used to store  the (key,value) pairs and a user reaches providers of a resource through the index, where the the access control on the incoming requests is enforced.
\par
However, in CAC, often the index is stored on untrusted nodes which are selected by the underlying overlay routing. Hence, unlike GAC systems, CAC-like systems leave the following information accessible to \textit{both} authorized and unauthorized users irrespective of access control policies set by the providers. 
\begin{itemize}
\item whether a given resource is shared in the system or not
\item the provider of a particular shared resource
\end{itemize}
In GAC, nodes have full control on the replies and hence can stop replying to unauthorized requests so that above information is accessed only by legitimate users. 
\par
Often such leakage of information to illegitimate users is referred as breaching the \textit{privacy} of the system. We say that CAC systems breach \textit{resource privacy} by allowing users to learn whether a given resource is shared in the system or not (from presence of corresponding $key$ in the index), and \textit{provider privacy} by allowing users to determine the provider of a shared resource (from the $value$ of the index entry) .
\section{Problem overview}
%Thus, CAC- systems breach privacy of the system which is inherent to GAC- systems. We refer them as \textit{resource} and \textit{provider} privacies through index with uncontrolled accesses.  Privacy often concerns with the leakage of information (e.g., from replies to search queries) to unintended or illegitimate users so that they can {\it learn} something from such a leakage, which is otherwise, accessible only by legitimate or intended users. Thus, CAC systems allow unauthorized users
% \begin{itemize}
% \item to learn whether a given resource is shared in the system or not ({\it resource privacy})
% \item to learn the provider\footnote{A provider is a node where access control function can be executed in a trusted way. Since in an untrusted environment, such a trusted node can not be any node other than the publisher, provider always refers to the publisher in our study.} of a particular shared resource ({\it provider privacy})
% \end{itemize}
% %The above list does not consider, for example, privacy of the searcher- who is searching for what resources in the system. 
%  Resource privacy is concerned with privacy of $key$\footnote{denoted with $K$ in the rest of the draft}, where as provider privacy is concerned with that of $value$\footnote{denoted with $V$} fields of a (key,value) pair. 
% \par
In this paper, we study the performance and privacy of CAC, GAC systems and propose a hybrid system referred as \textit{HAC} which is performance conscious and realizes both the resource and provider privacies. HAC is \textit{flexible} as it is highly configurable. It provides tunable knobs to the system designers which allow them to customize HAC which determines its position on privacy and performance graphs as shown in Figures \ref{pg} and \ref{comparison}. HAC is \textit{efficient} as it promises significant improvement in performance over GAC.
\par
It should be noted that GAC provides the highest privacy as it is index-free, but lacks in performance due to inefficient flooding routing, where as CAC provides improved performance with lack of privacy. 
\begin{figure}[htb]
\centering
\includegraphics[height=4cm, width=6cm]{Figures/privacy-graph.eps}
\caption{Privacy Graph}
\label{pg}
\end{figure}

% \par
% {\it Privacy} often concerns with the leakage of information (e.g., from replies to search queries) to uninteded or illegitimate users so that they can {\it learn} something from such a leakage, which is otherwise, accessible only by legitimate or intended users. We identify the following as the potential significant things a user can learn from such a leakage
% \begin{itemize}
% \item to learn whether a given resource is shared in the system or not ({\it resource privacy})
% \item to learn the provider of a particular shared resource ({\it provider privacy})
% \item to learn the contents of a resource shared in the system ({\it content privacy})
% \end{itemize}
% The above list does not consider, for example, privacy of the searcher- who is searching for what resources in the system. 
% \par
% Privacy preserving index is often used for sharing access controlled resources in the network \cite{privacyPreserving}. Users search the index to reach providers who host an interested resource, and the providers can enforce the access control policies on the resource access, which stops the illegitimate users from learning the contents of the resource. In the rest of the draft, we assume systems where AC is enforced always at provider nodes, and thus the content privacy is never breached. For example, in an access control aware Gnutella, the resource provider can verify the authorization credentials of the requesting peer and enforce the access control before returning the resource in reply to a search query. Hence, the paper deals with resource and provider privacies. 
% \par
%However, any form of index hosted on untrusted nodes incurs in a non-negligible amount of leakage. A P2P system with out any index can only (e.g. Gnutella) provide full privacy at the cost of performance, where as a system with index (e.g. Chord) provides no privacy but performs the best.  We propose an index-based P2P system which is performance- conscious at the same time, preserving the privacy of the index. We refer three such systems as {\it CAC}- Access Control aware Chord, {\it GAC}- Access Control aware Gnutella, {\it HAC}- a hybrid system which is close to GAC on privacy scale and CAC on performance scale, which is illustrated in Figure. \ref{comparison}.
\begin{figure}[htb]
\centering
\includegraphics[height=2.5cm, width=6cm]{Figures/comparison.eps}
\caption{Comparison of the systems on Privacy and Performance scale}
\label{comparison}
\end{figure}
%\section{Goal}
%The goal of the paper is to build a system that picks up merits of both GAC and CAC. As part of the design, it tries to minimize the information accessible to unauthorized users through index and maximize the performance. The system we envision (referred as \textit{HAC}\footnote{'H' for Hybrid}) is leverages on GAC by addressing information leakage and inherits the merits of CAC for its performance needs. 
\par
It should be noted that the issues of authorization policies, the format of the user identities and credentials are orthogonal to current scope of the paper. We assume that once a user reaches a resource provider, the user supplies identities and credentials appropriate to that provider needed for taking access control decision. 
\par
As a result, HAC infrastructure is \textit{flexible} as it can be used to share content controlled by completely different access control strategies. Providers with role based access control policies, providers with discretionary access control or social- network based access control, all can share the content with a single infrastructure. In summary, HAC realizes the following design goals:
\begin{enumerate}
 \item Index based system that allows to search through AC content.
\item Index that preserves both resource and provider privacies.
\item Configurable system enabling to choose various degrees of performance and privacy.
\item Flexibility to support heterogeneous access control policies. 
\item Complete autonomy to providers to decide on the type of policy.
\item Complete decentralization of index storage, network functions.
%\item Deniability of publishing a resource for a provider. Resistance to attempts by third parties to deny access to resources and index.
\end{enumerate}