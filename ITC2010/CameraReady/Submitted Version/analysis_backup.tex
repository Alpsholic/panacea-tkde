
\section{Analysis}
\label{sec:analysis}
In this section, we analytically study the privacy offered by PANACEA and estimate its communication overhead. We evaluate the privacy breach that can be achieved by a user who has {\em complete information} of the underlying system mechanism (PANACEA, structured or unstructured) and queries the system for a particular resource. We consider this as the worst case scenario for privacy breach with a single-query. Other cases where users have limited or no knowledge of the systems and case of attacks employing multiple queries to violate privacy of PANACEA, are discussed in Section \ref{sec:attacks}.%We compare the properties of our approach with those of the extreme cases of a completely unstructured (e.g. Gnutella) and of a fully structured P2P system (e.g. Chord) that offer access control. 

In the analysis, we employ probabilistic reasoning and use the following notation:
\begin{itemize} 
\item[i)] $P_{K}$ the probability for a user to deduce the existence of a certain {\em existing/shared} resource.
\item[ii)] $P_{V}$ the probability for a user to find the provider of a certain resource.
\item[iii)] $P_{-}$ the probability for a user to deduce the non-existence of a certain {\em non-existing} resource.
\end{itemize}
Moreover, we consider these probabilities separately, for the cases that a user is authorized or not (unauthorized), to access a copy of the requested resource and denote them as $P_{K,a}$, $P_{V,a}$ and $P_{K,u}$, $P_{V,u}$ respectively. Note that it does not make sense to consider separately $P_{-}$ for authorized and unauthorized users for non-existent resources. We use superscript $U$ and $S$ to denote metrics for unstructured and structured systems respectively, and a metric without any superscript is used for PANACEA i.e., $P_{K,u}^{U}$ refers to unstructured systems and equivalent metric for PANACEA is denoted by $P_{K,u}$.

\begin{definition}
An access-controlled system is said to provide \textit{higher} privacy if it promises: 
\begin{itemize}
\item[i)] Lower probabilities for $P_{K,u}$, $P_{V,u}$, which addresses an unauthorized user deducing a resource's presence and its provider.
\item[ii)] Lower probabilities for $P_{-}$, which addresses a user deducing a resource's non-existence.
\end{itemize}
\label{def}
\end{definition}

Under this definition of privacy, any privacy-enabling access control mechanism should try to minimize $P_u$, $P_{-}$. However, the search efficiency of the privacy-enabling mechanism should remain high, i.e. : a) $P_{K,a}$, $P_{V,a}$ should ideally be 1, which addresses authorized users ability to access the resources and discovering the providers, and b) the search communication cost $C_{s,a}$ should be kept low. 
\par
To this end, we require that the PANACEA should achieve the best of the unstructured and structured systems, w.r.t the privacy and search efficiency. Specifically, the \textit{privacy efficiency objectives} for PANACEA are: 
\begin{itemize}
\item $P_{K,u} \sim P_{K,u}^{U}$ and $P_{V,u} \sim P_{V,u}^{U}$ i.e., PANACEA should be closer to privacy-strong unstructured systems in the case of unauthorized searches
\item $P_{-} \sim P_{-}^{U}$ i.e., PANACEA should act like unstructured systems in hiding non-existence of resources
\item $C_{s,u} \sim C_{s,u}^U$ i.e., unauthorized searches should be as costly as that of unstructured systems
\item $C_{s,-} \sim C_{s,-}^U$
\end{itemize}
On the other hand, the \textit{search efficiency objectives} for PANACEA are:
\begin{itemize}
\item $P_{K,a} \sim P_{K,a}^S$ and $P_{V,a} \sim P_{V,a}^S$, i.e. PANACEA's privacy mechanisms should be transparent to authorized users like in privacy-free structured systems
\item $C_{s,a} \sim C_{s,a}^S$, i.e. the search performance of PANACEA should be closer to that structured systems
\end{itemize} 

Next, we analytically model the privacy and performance properties of the three systems for the cases where a user is authorized and unauthorized  to access an existing resource $r$. Let $N$ be the number of peers in the system and $N_c$ be the expected number of copies of $r$ in the system. $N_a\le N_c$ be the number of providers where the user is authorized to access $r$. For an existent resource $N_c\ge1$ and $N_a\ge 1$, $N_a=0$ for an authorized and unauthorized user, respectively. For non-existent resources, $N_c=0$. For simplicity, we assume, without loss of generality, that both $N_c$ and $N_a$ nodes are uniformly distributed in the network.

In an unstructured system, the search involves limited-hop flooding. Once a resource is found, if the user is authenticated, the query is directly replied to him. Otherwise, the query is further flooded. In PANACEA, if the requested resource that the user is authorized to access, is not indexed, he can discover its presence, only when at least one of the $N_a$ providers is contacted in the search process. We assume that the providers do not respond to search queries from unauthorized users, in order not to compromise the resource and provider privacies.

\subsection{Structured P2P systems}
As structured P2P systems have index available for all users, they offer no resource and provider privacies. Therefore,
\begin{equation}
P_{K,a}^S = P_{V,a}^S = P_{-}^S=P_{K,u}^S=P_{V,u}^S=1 
\end{equation}
The associated publishing and discovery costs $C_p$, $C_s$ for a single resource (copy) are given by:
\begin{equation}
C_{p,a}^S = C_{s,a}^S = C_{s,u}^S=C_{s,-}^S=O(logN)
\end{equation}

% \subsubsection{$\eta$}
% Since index entries are publicly accessible, there is no cooperation from any node is required.
% \begin{eqnarray*}
%  \eta^K  =  \eta^V = \eta^{-} = 0
% \end{eqnarray*}
\subsection{Unstructured P2P systems}
In \cite{unstructuredAnalysis}, authors present analytical models for unstructured P2P systems assuming a random graph overlay which are characterized by uniform degree distribution. For an average degree $d$, the number of distinct nodes $N'$ visited for a limited hop flooding with TTL set to $ttl$ can be computed as shown in \cite{unstructuredAnalysis}. Given $N'$, $P_{K,a}^U$ and $P_{V,a}^U$ are computed as follows. In unstructured P2P systems, these are always the {\em same} as resource and provider are discovered at the same time.
\begin{equation}
P_{K,a}^U =P_{V,a}^U= 1- \left( 1- \frac{N'}{N}\right)^{N_a}  
\end{equation}
We realize that computing $N'$ and {\it expected search cost} $C_{s,a}^U$ and $C_{s,u}^U$ for power-law graphs is a research problem itself. 
However, the expected publication cost 
\begin{eqnarray}
C_{p,a}^U  =  0
\end{eqnarray}
Since no provider would reply to unauthorized queries, the presence of the resource can never be deduced. Therefore, these systems offer the highest resource and provider privacies. 
\begin{equation}
P_{K,u}^U =  P_{V,u}^U =  0
\end{equation}
Regarding the deduction of the non-existence of a particular resource, it suffices to calculate the probability to discover if a {\em single} copy exists into the system. However, observe that discovering a non-existent resource is similar to discovering an unauthorized existent resource in the unstructured P2P system. Thus, non-existence can be deduced with probability: 
\begin{equation}
P_{-}^U = P_{K,u}^U 
\label{pminus_u}
\end{equation}
\subsection{PANACEA}
Recollect that if the user finds an $(m,n)$-entry for $r$ in the DHT, he contacts all the providers listed as part of the search. Hence, had the index been published by any of the $N_a$ nodes where he is authorized, we say that he can deduce the provider with probability $1$. The probability that at least one of $N_a$ nodes publish into the DHT is $1-(1-\mu)^{N_a}$. On the other hand consider the case where the index entry is published by none of the $N_a$ nodes (probability of which, is $(1-\mu)^{N_a}$) but by some $i$ of remaining $N_c-N_a$ number of nodes where the user is not authorized to. Here, the user employs flooding also in addition to contacting  $i\cdot n$ number of providers listed in the DHT entry. Here, we say that the user could deduce the provider with the probability $\frac{1}{i\cdot n}$ in addition to what is possible in flooding (i.e., $P_{V,a}^U$). However, the number of providers is less than $i\cdot n$ because of collisions in provider lists across multiple $(m,n)$ entries. We account for this with a \textit{collisoin factor} $f_v$. When the DHT entry is not present for the key (probability of which is $(1-\mu)^{N_c}$), the user can deduce the provider with what is possible in flooding. Hence,
\begin{equation}
\begin{split}
P_{V,a} = & \left[1-(1-\mu)^{N_a}\right] \cdot 1  +  \\
& (1-\mu)^{N_a} \cdot \sum_{i=1}^{N_c-N_a}{{N_c-N_a}\choose{i}}\mu^i (1-\mu)^{(N_c-N_a-i)}\\
&\cdot \left[\frac{1}{(i\cdot n)(1-f_v)} + P_{V,a}^U\right]+(1-\mu)^{N_c}\cdot P_{V,a}^U
\end{split}
\label{pva_panacea}
\end{equation}
The parameter $f_k$ is the probability of {\em key collisions} in the key lists of the different $(m,n)$-index entries in the DHT when $N_r$ resources are stored in the system and it is derived as the number of $m$ disjoint partitions of $|R|-N_r$ resource keys over the number of their total combinations into sets of size $m$. If the requested resource is not published into the DHT, the user learns about the resource with probability $P_{K,a}^U$.  This reasoning is captured in the following equation:
\begin{equation}
\begin{split}
P_{K,a} = & \left[1-(1-\mu)^{N_a}\right] \cdot 1  +\\ 
& (1-\mu)^{N_a} \cdot \sum_{i=1}^{N_c-N_a}{{N_c-N_a}\choose{i}}\mu^i (1-\mu)^{(N_c-N_a-i)}\\
& \cdot \left[\frac{1}{(i(m-1)+1)(1-f_k(N_r))}+ P_{K,a}^U\right] + (1-\mu)^{N_c}\cdot P_{K,a}^U \>,\\
&~\textrm{where}~~f_k(N_r) = \frac{\frac{|R|-N_r}{m}}{{{|R|-N_r}\choose{m}}}
\end{split}
\label{pka_panacea}
\end{equation}
Recall that, for each $(m,n)$-index entry insertion, $m$ number of index entries have to be inserted into the DHT, each incurring a cost of $C_{p,a}$. The searching cost depends on whether one of $N_a$ nodes published into the DHT or not. If none of them published, $C_{s,a}^U$ should be accounted for. Each search also incurs the look up cost on the DHT. Hence, the expected publishing and searching costs are given by:
\begin{equation}
\begin{split}
C_{p,a} & = \sum_{i=1}^{N_c}{{N_c}\choose{i}}\mu^i(1-\mu)^{(N_c-i)}\cdot i\cdot \left(\frac{1}{\lambda}+ m \cdot C_{p,a}^S\right) \\
C_{s,a} & = C_{s,a}^S +(1-\mu)^{N_c}\cdot C_{s,a}^U+ \sum_{i=1}^{N_c}{{N_c}\choose{i}}\mu^i(1-\mu)^{(N_c-i)}\\
&\cdot\left[i\cdot n\cdot(1-f_v)+ \left(1-\frac{N_a}{N_c}\right)^{i} C_{s,a}^U\right]
\end{split}
\label{csa_panacea}
\end{equation}

We can derive $P_{K,u}, P_{V,u}$ from (\ref{pka_panacea}) and (\ref{pva_panacea}) respectively by observing that for an unauthorized user $N_a=0$ and replacing $P_{K,a}^U$,$P_{V,a}^U$ with $P_{K,u}^U$,$P_{V,u}^U$ respectively.
%\begin{equation}
%\begin{split}
%P_{K,u} = & \left[\sum_{i=1}^{N_c}{{N_c}\choose{i}}\mu^i(1-\mu)^{(N_c-i)}\frac{1}{i\cdot(m-1)+1}\right] +\\
%& (1-\mu)^{N_c}\cdot P_{K,u}^U\\
%P_{V,u} = & \left[\sum_{i=1}^{N_c}{{N_c}\choose{i}}\mu^i(1-\mu)^{(N_c-i)}\frac{1}{i\cdot n}\right] +\\
%& (1-\mu)^{N_c}\cdot P_{V,u}^U
%\end{split}
%\end{equation}

The expected searching cost for an unauthorized user is given by:
\begin{equation}
\begin{split}
C_{s,u} = & C_{s,u}^S +(1-\mu)^{N_c}\cdot C_{s,u}^U+\\
& \sum_{i=1}^{N_c}{{N_c}\choose{i}}\mu^i(1-\mu)^{(N_c-i)}\left[i\cdot n\cdot (1-f_v) + C_{s,u}^U\right]
\end{split}
\end{equation}

Finally, we derive $P_{-}$, i.e. the probability to deduce the non-existence of a non-existent resource. Given an event space $\Omega=\{${{\em Find}, {\em NotFind}$\}$, representing that a non-existent resource is found or not in the DHT respectively, the probability of non-existence ($Pr(NE)$), as deduced by a user, is given by: 

\begin{equation}
\begin{split}
P_{-} = Pr(NE\mid\Omega) =  & Pr(NE\mid NotFind)\cdot Pr(NotFind)\\
& +Pr(NE\mid Find)\cdot Pr(Find)\>,\textrm{where}\\
%Pr[\textrm{Not Find}]& =  1-\left[\mu\cdot\frac{1}{m}+(1-\mu)P_{K,u}^U\right] \\
Pr(NE\mid NotFind) =  & P_{K,u}^U \\
Pr(NE\mid Find) =  & \frac{m-1}{m} \\ 
%Pr[\textrm{Find}] &= \left[1-\left(1-\frac{1}{|R|}\right)^{\mu N_r (m-1)}\right] + P_{K,u}^U 
Pr(Find) = & \left[1-\left(1-\frac{1}{\mid R\mid}\right)^{\mu N_r (m-1)}\right] \\
Pr(NotFind) = & 1 - Pr(Find)
\end{split}
\label{pminus_panacea}
\end{equation}
$N_r$ is the total number of resources in the system and $R$ is the resource namespace. $Pr(NE\mid NotFind)$ expresses the probability that a resource is non-existent, given that it is not found in the DHT. This is similar to the probability of deducing the existence of an unauthorized resource for a user in unstructured P2P systems, because an existing resource is same as a non-existing resource for an unauthorized user. $Pr(NE\mid Find)$ is the probability that the resource corresponding to the key does not exist. $Pr(Find)$ expresses the probability that an arbitrary resource name from space $R$ may have been inserted into the index, while $Pr(NotFind)$ is the complement of $Pr(Find)$. Observe that $P_{-}$ is minimal ($\sim 0$) for reasonable values of the various parameters.

Finally, we estimate the expected query cost to deduce the non-existence. The user first searches for an index entry and then employs  flooding, hence,

\begin{equation}
 C_{s,-} = C_{s,-}^S + C_{s,-}^U
\label{csminus_panacea}
\end{equation}


