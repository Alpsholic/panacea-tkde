\section{Privacy analysis}
The privacy achieved for a single query in the case of a single adversary acting as a searcher was presented in \cite{itc}. The case of a more general and stronger adversary is addressed in the following: An adversary either being a participant in randomized routing or a host node in the DHT, can accumulate a finite number of $(m,n)$- entries. It is possible, some times, for a searcher too to construct the set of $(m,n)$- entries that contributed to a particular entry in the DHT, even though, the overlaps in the set of providers across entries prevent from doing so. Hence, it is essential to understand the resource and provider privacy offered by a set of \textit{(m,n)-}entries, which are close to that of a single \textit{(m,n)-}entry case, where the resource existence can be established with a probability of $\frac{1}{m}$ and provider with a probability of $\frac{1}{mn}$.
\begin{theorem}
For an unauthorized adversary $v_k$, the offered resource and provider privacies by an arbitrary set of $(m,n)$-entries \texttt{E} for a resource $r$, respectively, are 
\begin{itemize}
 \item $1$ if the set $E$ contains $m.n$ number of $(m,n)$-entries with complete overlap in their provider sets,
\item $\frac{1}{m}$ and $\frac{1}{m(n-o)}$ if $E$ contains larger number of $(m,n)$-entries with $o$ number of common providers,
\item $\frac{1}{m}$ and $\frac{1}{mn}$, otherwise (or $\frac{1}{m(n-1)}$ if the user $v_k$ sees herself in the provider set).
\end{itemize}
\label{theorem:privacyforset}
\end{theorem}
\begin{IEEEproof}
 We represent the set \texttt{E} as
\begin{equation}
\begin{split}
%\texttt{E} = \bigcup_{j=1}^{MAX} \texttt{e}_j \textrm{, where} \\
\texttt{E} = \bigcup_{j=1} \texttt{e}_j \textrm{, where }
 \texttt{e}_j =(K_j,V_j) \textrm{ is the } j^{\textrm{th}}\textrm{entry}
\end{split}
\end{equation}
The set \texttt{E} can be divided into two subsets $\texttt{E}_1$ and $\texttt{E}_2$ such that 
 $\texttt{E} = \texttt{E}_1 \cup \texttt{E}_2$, where
\begin{equation*}
\begin{split}
%\texttt{E} = \texttt{E}_1 \cup \texttt{E}_2 \textrm{ ,where}\\
\texttt{E}_1 = \bigcup_{j} \texttt{e}_j: r \in K_j \textrm{ and }
 \texttt{E}_2 = \bigcup_{j} \texttt{e}_j: r \notin K_j
\end{split}
\end{equation*}
For the user $v_k$, the set $\texttt{E}_2$ can not reveal any information about resource $r$, hence, the rest of the discussion considers only the set $\texttt{E}_1$. As discussed earlier, all $K_j\in E_1$ are similar. Given this, the possible cases that arise are:
\begin{enumerate}
 \item \textit{Case-I:} All of the sets $V_j$ are identical.
\item \textit{Case-II:} All of the sets $V_j$ are distinct.
\item \textit{Case-III:} A set of values repeat in multiple sets $V_j$s.
\end{enumerate}
\underline{Case-I:} When the set $V_j$s are the same, the adversary must be either a participant in the randomized routing or a host node in order to know about multiple identical \textit{(m,n)-}entries. This case is particularly interesting when $\mid \texttt{E}_1 \mid=m\cdot n$, which signifies that all the $n$ providers listed in the entries own all the $m$ resources listed. The resource and provider privacies are completely breached in this case, thus resource and provider privacies are said to be $1$. 
\par
Consider the case when $\mid \texttt{E}_1 \mid \geq n\cdot (m-1)+1$. Out of all the possible odds, the adversary could be more inclined to believe that the set $E_1$ corresponds to $m-1$ resources being published by all $n$ providers so far and the $m^{th}$ resource which should be $r$, is about to start appearing in the index. Hence, when $\mid \texttt{E}_1 \mid$ is exactly $n\cdot (m-1)+1$, the adversary can be certain that $r$ exists in the system and thus resulting in a resource privacy of $1$. %\footnote{Privacy refers to the probability of a resource being present in the system.}. 
In this case, the provider privacy is $1/n$ and increases towards $1$ in steps of $1/n$ for each additional entry in the set $E_1$.
\par
Similar analysis can be done for the other cases where $\mid \texttt{E}_1 \mid < n\cdot (m-1)+1$, but resource and provider privacy is not breached in this case. 
%For all other $\mid \texttt{E}_1 \mid$, the resource and provider privacies achieved are $\frac{1}{m}$ and $\frac{1}{mn}$ respectively
%tvas individual \textit{(m,n)-}entries $\texttt{e}_j$s are indifferentiable among each other. Hence, the privacies offered by a single \textit{(m,n)}-entry apply to the set $\texttt{E}_1$. Thus, the resource and provider privacies achieved are $\frac{1}{m}$ and $\frac{1}{mn}$ respectively.

\underline{Case-II:} Since all $V_j$s are distinct, there is no reason for the adversary to infer why one provider is more probable than the other, for being the valid provider in the list. Hence, no additional provider breach can occur because of availability of a set of \textit{(m,n)-}entries ($\texttt{E}_1$), and hence the provider privacy is said to be still $\frac{1}{mn}$.
\par
We discuss the case of resource privacy here. When $\mid\texttt{E}_1 \mid$ number of \textit{(m,n)-} entries are observed by the adversary, he can take odds on different possibilities ranging from the case of $\mid\texttt{E}_1 \mid$ number of providers publishing the same resource from the set of $m$ resources, to the case of each resource being published with different multiplicities. 
\par
Consider the latter case, where resources are published in varied multiplicities by the providers. There are several possible events. Lets represent each event as a vector of size $m$, with $i^{th}$ entry in the vector representing the number of occurrences of resource $r_i$ in $\mid\texttt{E}_1 \mid$ (denoted by $e$) publications. Hence, some of the possible events in this case are: $[0,0,..,e]$, $[0,0,..,1,e-1]$, $[0,0,.., 2,e-2]$, ..., $[e,0,..,0]$. This can be modeled as the arranging exactly $e$ balls into $m$ boxes where boxes are allowed to be empty. This can be done in ${e+m-1}\choose{e}$ number of ways, which is equivalent to the number of terms in the multinomial formula $(x_1+x_2+...+x_m)^e$ where $m$ and $e$ are positive integers. Out of ${e+m-1}\choose{e}$ events, there are ${e+m-2}\choose{e}$ events where a particular resource has $0$ occurrences. In all the remaining events, the resource appears at least once, and hence one can attribute a resource privacy of $1-\frac{{{e+m-2}\choose{e}}}{{{
e+m-1}\choose{e}}}$.
\par
In the former case where $e$ number of publications of a single resource contributed to the observed list of $(m,n)$- entries. However, since the resource $r$ is not differentiable with $m-1$ other resources, the resource privacy achieved would be $\frac{1}{m}$, irrespective of the size of the set $E_1$. %Higher the size of this set and hence, higher the number of occurrences of same keys reveal only the popularity class of the resource as mentioned earlier, and not about their presence in the system.
%\par
%But, while making recommendations to the dictionary, the providers blend a genuine resource into a set of resource names chosen randomly from a local dictionary, and hence, an anonymization set chosen in FS-approach, most likely has only one genuine resource. The probability of having more than one genuine resource is negligibly small. On the other hand, because of the way the anonymization is done, one can not infer differentiate one resource over the other by just inspecting the contents of an \textit{(m,n)-}entry. Hence, we argue that the system achieves a resoure privacy of $\frac{1}{m}$ in this case too.
% However, because of repeated presence of $r$ in $K_j$s, the adversary can infer that it is more probable for $r$ to be genuine than not. But, with clever resource anonymization techniques (as discussed in Section. \ref{sec:}), PANACEA ensures that a phantom resource key can also get repeated and the number of such repetions is comparable to that of a genuine key. Number of such repetitions defines the value of $n_f$: the maximum possible provider list a phantom resource can take. Hence, as long as the combined provider list ($\mid V\mid$, where $V=\bigcup_{j}V_j$) is shorter than $n_f$, the resource $r$ is as frequently seen as a phantom resource in the \textit{(m,n)-}entries. Hence, the adversary's belief on $r$'s existence is due to \textit{partial} view of the system presented by the set $\texttt{E}_1$. However, assume the case of an adversary who could approximte $n_f$ with the help of external information. Such an advesary can infer that $r$ is genuine with a higher probability in case $\mid V\mid > n_f$. In that case, the resource privacy is breached and its existence is revealed. As a side effect, the provider privacy is affected which would be $\frac{1}{n}$ for $r$.


\underline{Case-III:}  Let's assume that a set 
%$S_k \subset K$ (where $K=\bigcup_{j}K_j$) occur multiple times (say $l$ no. of times) in $K_j$s. Two possibilities exist for the corresponding $V_j$s: they are distinct or a set 
 $S_v \subset V$ (where $V=\bigcup_{j}V_j$ and $\mid S_v \mid<n$) appears in multiple (say $l$ number of) $V_j$s. Adversary as a searcher can not find out such overlaps.
% \par
% Here, we argue that the repitition of the set $S_k$ of keys does not reveal more information about $r$'s existence for the following reason: When $r$ is seen in multiple $K_j$s, it signifies the multiplicity of $r$'s occurrence in the \textit{(m,n)-}entries which helps an adversary only to differentiate $r$ from a resource with lesser number of occurrences, not in inferring about its existence as long as its multiplicity is within the bound of maximum possible multiplicity for a phantom key (which determines $n_f$). 
% \par
% If $S_k=\{r\}$, then $r$'s resource privacy can be quantified as:
% \begin{equation*}
%  \frac{l}{l(m-1)+1} \\
% = \frac{1}{m} \textrm{ as } l\rightarrow \infty
% \end{equation*}
% \par
% For the case of distinct $V_j$s, the provider privacy is not breached beyond $\frac{1}{mn}$. If a set $S_v$ of providers are observed $l$ number of  times, 
 In this case, one can argue that the user $v_k$ may try to attribute the resource $r$ to the providers in $S_v$ with higher probability than to other providers. The provider privacy achieved from the above $l$ number of \textit{(m,n)-}entries is:
%If $\mid S_v\mid=1$, then this provider's privacy can be quantified as:
\begin{equation*}
\begin{split}
%  \frac{l}{m\cdot l(n-1)+1} 
% = \frac{1}{mn} \textrm{ as } l\rightarrow \infty \\
 \frac{l}{m\cdot \left(l(n-\mid S_v \mid)+\mid S_v \mid\right)} 
= \frac{1}{m\left(n-\mid S_v \mid\right)} \textrm{ , }
\end{split}
\end{equation*}
for large values of $l$. For maximum size of $S_v$ in this case, which is $n-1$, the provider privacy as per the above formula is $\frac{1}{m}$. The resource privacy is still the same ($\frac{1}{m}$), as explained above.
%\par
%Since a provider (say $v_1$) is typically assumed to publish a resource only once, presence of the same provider in multiple \textit{(m,n)-}entries for $r$ could mean that $v_1$'s presence in other entries can not reveal more about $v_1$ with respect to $r$. The presence of $v_1$ is more due to either $v_1$ publishing other resources appearing in the \textit{(m,n)-}entries or other providers selecting $v_1$ as part of provider anonymizations. 
\end{IEEEproof}
% \textbf{Discussion:} Here, we discuss the case-I and III of above theorem in detail and argue that, in practice, probability of occurrence of such cases is negligible. Hence, in general, the PANACEA system at large, promises resource and provider privacies of $\frac{1}{m}$ and $\frac{1}{mn}$.
% \par
% It must be noted that provider anonymization of PANACEA picks up different set of providers for successive resouce publications by an individual publisher. Thus, the size of the overlaps in providers across multiple \textit{(m,n)-}entries is minimized. Hence, it is highly unlikely that a single participant in the randomized routing accumulating many \textit{(m,n)-}entries with the same providers repeated for a given resource. Moreover, the way the pStores are populated with providers from passing \textit{(m,n)-}entries can be attributed to the observed repetition of the same provider across \textit{(m,n)-}entries.
% \par
% Lets assume that there are $N_c$ providers willing to publish a resource $r$. Assuming that all these providers have similar pStores of size $N'$ (which is worst possible case in this context), the probability that all of the $N_c$ providers choose the same set of providers of size $o$ for their \textit{(m,n)-}entries is: 
% \begin{equation*}
% \begin{split}
% \frac{1}{\left[\binom{N'}{o}\binom{N'-o}{n-o}\right]^{N_c}}
% \end{split}
% \end{equation*}
% Moreover, note that the probability of multiple pStores having the same set of entries is negligibly small because of the way they are populated. 
% Further, the privacy breach of case-I is limited to only the participants in the randomized routing and the host node for the resource key. For adversaries as searchers, the system behaves like the case-II.
% \begin{theorem}
% For an unauthorized adversary as a \textit{searcher} or a \textit{host node} or a \textit{participant in randomized routing} for a resource $r$ published into the DHT, the offered resource and provider privacies by the system are $\frac{1}{m}$ and $\frac{1}{mn}$ respectively.
% %Every node participating in RR routing, deduces the valid provider from an RPP entry with only probability $\frac{1}{n-1}$.% even if the previous hop appears in the provider list.
% \end{theorem}
% \begin{IEEEproof}
% This is a special case of the advesary in Theorem. \ref{theorem:privacyforset}. The adversary with any or all of the mentioned roles can construct all \textit{(m,n)-} entries where the resource $r$ appears, as is the case with FS-approach of anonymization. However, as per Theorem. \ref{theorem:privacyforset}, such a set of \textit{(m,n)-}entries offer resource and provider privacies of $\frac{1}{m}$ and $\frac{1}{mn}$ respectively.
% %We prove by mathematical induction on the number of times $k$ the RPP entry is seen by the same node (say $n_j$) as part of RR routing.
% %We analyse the case for a hop $n_i\rightarrow n_j$. AND for repitions? The following are the possible cases.
% %\textit{CASE-1:}
% \end{IEEEproof}
% % \begin{theorem}
% % The privacies achieved by a node in RR, a host node, and a searcher node are repsectively.. 
% % \end{theorem}
% % \begin{proof}
% % \end{proof}                                                                                                                                                                                                                                                                                                   
% \subsubsection{Iterative attacks} 
% In the case of iterative attacks, an adversary, repeatedly queries the DHT for an interested resource. Note that, by querying the DHT, the adversary can not always construct the \textit{(m,n)-}entries that contributed to the DHT entry, because the overlaps in the provider lists across multiple entries can not be noticed after the lists are merged. Even if such entries are computed, say in case there are no overlaps in the provider lists, the possible privacies offered are  as discussed in the Theorem. \ref{theorem:privacyforset}.
% \subsubsection{Collusive group of adversaries}
% A group of peers in the system will collaborate among themselves in order to breach the privacy offered by the system. For example, when a peer constructs an \textit{(m,n)-}entry, if \textit{n-1} number of providers listed collaborate with each other, finding the genuine provider becomes trivial for these colluders. 
% \par
% Users in a collusive group are assumed to share the following information with other group members: a provider appearing in an \textit{(m,n)-}entry is due to its own publication event or a mere selection by some other provider as part of anonymizaiton. In the latter case, the size of the anonymity set reduces by $1$ for the colluders. Given this, the effective anonymity set size decreases by the number of members of a collusive group that appear in a single \textit{(m,n)-}entry. It should be noted that if collusive groups are non-overlapping, there is no harm if members of two separate groups are included in a single \textit{(m,n)-}entry, as there is no collusion among such groups. For example, if one member from each group is selected into the entry, the privacies offered will be still the same.
% \par
% We assume the case of \textit{static} collusive non-overlapping groups of maximum size $c$. We observe that typically collusive users are socially connected and share mutual trust which is limited to only the members inside the group. In general, such groups are limited in size as larger the group is, more difficult it would be to conceal the actions of the group members from the rest of the members in the system.
% \par
%  For $c=n-1$, provider privacy can be breached successfully if and only if a provider chooses a particular set of $n-1$ colluders for an \textit{(m,n)-}entry. However, probability for this to occur is $\frac{1}{{{N'}\choose{n-1}}}$ which is $\sim 0$ for reasonable values of $N'$ and $n$, where $N'$ is the size of the pStore. Hence, as long as $N'$ is significantly larger than $c$, a collusive group of adversaries can not breach provider privacy.
% \par
% Note that this type of attack is concerned with only the provider privacy and resource privacy will not be breached by a colusive group. More over,
% the privacy breaches are limited to only members of such collusive groups. The system's privacy guarantees do not alter for other adverseries in the system.
% \textcolor{red}{Special attack all c members into a single pstore- its easy}.
% \textit{/* TODO */}
% \begin{itemize}
%  \item PANACEA's resistance to such a case- random selection of peers in anonymization...
% \item get old equations here
% \end{itemize}
% \par
% \subsection{A powerful omni-potent adversary}
% Requires a significant amount of resources to monitor 
% An adversary who has access to all the index entries can approximate how many resources are published by a particular provider, but can not determine which resources? 
% \begin{itemize}
%  \item Attacker with global knowledge of the DHT indexes (global index)- who knows the contents of pStores, complete perspective of traffic/communication among peers.
% \item Global statistics- what an attacker can learn if he can accumlate all the entries in the DHT.
% \end{itemize}
%\subsubsection{Attacker as an authorized user}
