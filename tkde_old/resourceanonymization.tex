\par
In the following section, we explain the process of resource anonymization in detail.
\section{Resource anonymization}
\label{sec:resource}
When a resource $r$ is selected for publishing, there exist two possibilities for anonymizing the resource: (i) anonymize the resource with \textit{random} set of keys (ii) anonymize the resource with \textit{fixed} set of keys. Both of these approaches are discussed in detail in the following section and referred as \textit{anonymization by random selection} and \textit{anonymization by fixed selection} respectively. Later in the following discussion section, we highlight the differences between both the approaches.
\subsection{Anonymization by random selection (RS) approach}
\todo{/** This part will be removed from the journal submission to save space. This approach is well discussed in our ITC paper. **/}\\
In this approach, the resource name ($r\in R$) is anonymized i.e., the resource name $r$ is blended into $m-1$ number of other resource names. Then hash keys of all the $m$ entries are published into the DHT. 
\par
These resource names are randomly selected from the resource namespace $R$, which can be domain-specific or span multiple domains and even contain words and their combinations from a dictionary. An adversary that is able to observe the resource keys of the $(m,n)$-entry may be able to derive the corresponding resource names of the $m-1$ keys (i.e., preimages) by employing a dictionary attack. which is considered as infeasible especially against some strong hash functions such as SHA-1. In spite of dictionary attack, the adversary can not deduce which of these keys corresponds to genuine resource.
\par
When multiple providers try to publish same resource $r$, since individual anonymization processes are independent, each may end up selecting non-overlapping random entries as part of anonymization. In contrast, the following anonymization technique forces all the providers choose the same fixed set of entries for anonymizing $r$.
\subsection{Anonymization by fixed selection (FS) approach}
\label{sec:circular}
%\todo{Is this too much detailed?}
Instead of anonymizing the resource name by resource names picked randomly from a dictionary, the FS mechanism picks a fixed set of resource names for anonymization. It blends the resource key into $m-1$ number of keys\footnote{Note that hashes of these keys are published into the DHT.} in such a way that the same keys are chosen every time the resource is published by different providers in the network. In order to achieve that the FS approach mandates that the same dictionary $D$ must be used by all the providers in the system, in addition to selecting the keys for anonymization from the dictionary as specified in the following. To realize this, an {\it (m,n)-} entry with keys other than the ones present in the current dictionary will be dropped by peers during randomized forwarding.
\par
The dictionary $D$ is a arranged as a circular ordered list of size $2^h$ with $h$-bit index. The way the list is ordered is explained later. The dictionary supports the following operations related to retrieval of keys from the dictionary:
\begin{itemize}
 \item \textit{$Lookup_D(k)$:} returns the index $i \in [0,2^h-1]$ corresponding to the position of key $k$ in the dictionary $D$.
\item \textit{$getEntry_D(i)$:} returns the key located at index $i$ in the dictionary $D$.
\end{itemize}
\par
Assume the dictionary $D$ is represented by a circle as illustrated in Figure \ref{fig:circular}. The anonymization algorithm selects $\lceil\frac{m}{2}\rceil$ number of diameters on this circle and the keys located at indeces represented by the corresponding diameter end points, form the $m$ number of keys required. The diameters are cleverly chosen in the following way: the \textit{initiator diameter}- the diameter passing through the resource to be anonymized, is chosen first. Then $\lceil\frac{m}{2}\rceil-1$ number of other diameters are chosen such a way they are uniformly spaced on the circle separated by a fixed angular distance $\theta=\frac{360^\circ}{\lceil\frac{m}{2}\rceil}$. As a result, at the end of anonymization process, there is no way to figure out the initiator diameter from which the other diameters are based on. Every diameter is equally likely to be an initiator. The algorithm is more formally explained here, which finally outputs the set $S_m$, a set of $m$ keys: 
\par
Now we introduce the function $getIndex(i,\alpha)$ which outputs the index $j$ at an angular distance of $\alpha$ from $i$ on the above circle. If a $h$-bit identifier space resulting in a key space of $2^h$ size mapped onto the circle, thus resulting in an angular distance of $\frac{360^\circ}{2^h}$ between two successive points. Hence,
\begin{equation*}
 j = \left(i + \frac{\alpha}{\left(\frac{360^\circ}{2^h}\right)}\right)\textrm{ mod }2^h 
\end{equation*}
For example, in the Figure. \ref{fig:circular}, $j = i + 2^{h-1}$ as $j$ is at an angular distance of $180^\circ$ from $i$.
\begin{figure}
\centering
\includegraphics[height=4cm, width=4cm]{Figures/circularapproach.pdf}
\caption{Circular approach for key anonymization}
\label{fig:circular}
\end{figure}
% \begin{algorithm} 
% \begin{algorithmic}[1]
% \STATE Let $k_1$ be the key on the ring correponding to the resource $r$ to be anonymized. $S_m=\{k_1\}$
% \STATE Choose the initiator diameter passing through $k_1$. Add $k_2$ to $S_m$. 
% \STATE Let \textit{count=1}
% \WHILE{$count \leq \lceil\frac{m}{2}\rceil $} 
% \STATE choose the diamater $(k_i,k_j)$ such that $k_i$ is at an angular distance of $\theta=\frac{360^\circ}{\left(\frac{m}{2}\right)}$
% \STATE add $k_i$ and $k_j$ to the set $S_m$
% \STATE count = count + 1
% \ENDWHILE
% \end{algorithmic}
%  \label{algorithm}
%  \caption{The circular approach}
% \end{algorithm}
\begin{algorithm} 
\begin{algorithmic}[1]
\STATE Let $r$ be the resource to be anonymized and $i=Lookup_D(r)$
\STATE $S_m=\phi$
\WHILE{$\mid S_m\mid < m$}
\STATE $j=getIndex(i,180^\circ)$
\STATE add $getEntry_D(i)$ and $getEntry_D(j)$ to $S_m$
\STATE $i=getIndex(i,\theta)$ where $\theta=\frac{360^\circ}{\lceil\frac{m}{2}\rceil}$ CHANGE IT m NOT m/2
\ENDWHILE
\IF {$\mid S_m\mid > m$}
\STATE remove a member from $S_m$ other than $r$ randomly 
\ENDIF
\end{algorithmic}
 \label{algorithm}
 \caption{The circular approach}
\end{algorithm}
If $m$ is odd, one key from the resulting $m-1$ keys, is randomly picked and removed from the set\footnote{The side effect of this on the resource privacy is not dealt in this paper. An even value can be used for $m$ in order to avoid this.}. 
\subsubsection{Dictionary construction and distribution}
In the following we discuss the issues related to dictionary construction and realizing FS approach for anonymization.
\par
It is obvious from the FS description that in order to have fixed set of keys to be chosen by independent providers of the same resource, all the providers must be using the same dictionary. If PANACEA system is used in resource sharing environments where a dictionary can be constructed apriori to deploying the system covering complete namespace used for naming the resources, a static dictionary can be built into the PANACEA distributions, thus guaranteeing the same dictionary used by all providers in the system. 
\par
However, in a typical resource sharing use case, predefining such a dictionary with all possible resource names is impractical. Hence, a dictionary construction mechanism is required which has to meet the following requirements:
\begin{enumerate}
 \item \textit{Supporting updates:} Dictionary must be expandable over time.
\item \textit{Preserving diameter end points:} The keys falling in the indeces corresponding to diameters end points must not alter as the dictionary evolves over time. 
\end{enumerate}
In addition, we need a \textit{dictionary distribution mechanism} in order to distribute the constructed dictionary to all the providers in the system.
\par
In the following, we propose a simple dictionary distribution mechanism which assumes the presence of a single \textit{dictionary manager}: a provider who performs the dictionary construction and distribution. This dictionary manager can be any well-known peer in the system or a web server whose credentials are in built into PANACEA client distributions. It should be noted that the dictionary manager has no more knowledge than an unauthorized adversary, which will become explicit shortly, and hence, the PANACEA system achieves the same resource and provider privacies for the dictionary manager as that of an unauthorized user. The only intended requirement that dictionary manager has to meet is: to implement the steps involved in the dictionary construction mechanism in a faithful way. Indeed, it is expected so as dictionary manager can not benefit w.r.t privacy by behaving otherwise.
\par
The dictionary construction process involves the following steps:
\begin{enumerate}
 \item Collecting \textit{dictionary recommendations} from providers
\item \textit{Committing} the recommendations to the dictionary
\item \textit{Distributing} the updated dictionary to the providers
\end{enumerate}
These steps are discussed elaborately here.
\par
%\subsubsection{Dictionary recommendations}
\textit{-\underline{Dictionary recommendations}}: The providers in the system propose recommendations to the dictionary maintained by the dictionary manager. Each recommendation consists of a set of keys to be added to the current dictionary. We assume that this set of keys is chosen randomly from a provider's local dictionary or manually fed by the user through application interfaces. Providers submit the recommendations at arbitrary points of time with involving keys not necessarily related to the resources they want to publish. However, recommendations can be made when the name of the resource the provider wants to publish is not covered by the current dictionary.
\par
The recommendation requests are submitted to the dictionary manager using anonymized routing like in Crowds \cite{crowds}. This anonymous routing prevents the dictionary manager from attempting to breach provider privacy by finding the correlations between dictionary recommendations and provider lists in the DHT of keys appearing in the recommendations.
\par
As part of the anonymous routing, the intermediate peers can also contribute a set of keys to the recommendation request. Optionally, in order to prevent the intermediate peers learning from the key list present in the request, the keys can be encrypted with public key of the dictionary manager.
\par
In the evaluation, we assume the providers postpone the publication of a resource until the resource is covered by the current dictionary. However, providers need not wait till their recommendations committed and can publish a resource using RS-anonymization. We skip this step in our evaluation.
\par
\textit{-\underline{Committing recommendations}}: The dictionary manager accumulates all the recommendations and \textit{commits} them to the dictionary	by inserting the keys contained in the recommendations to the dictionary. This step may need to expand the current dictionary in size which must preserve the end points of diameters in old dictionary as specified earlier. In the following, we propose one such committing mechanism. 
\par
The dictionary is first doubled in size by creating equal no. of free slots with each free slot interleaved between two consecutive elements of original dictionary. Lets denote the new dictionary as $D'$ and the original as $D$. It should be noted that entries in $D'$ are identified with index of size $h+1$ bits. Moreover, the altered positions of keys in $D$ in $D'$ is given by
\begin{equation}
\begin{split}
\forall k\in D, Lookup_{D'}(k) = 2\cdot Lookup_{D}(k).
\end{split}
\label{eq:newindex}
\end{equation}
The dictionary manager then places the keys from each recommendation randomly into the newly created free slots excluding the keys which are already present in the current dictionary. This random placement prevents a certain provider controlling the diameter end points thus possibly compromising the resource privacy. For example, a malicious provider may target a particular resource and decide to compromise its resource privacy. Then he carefully crafts some bogus keys and submits a dictionary recommendations with all these keys. If all of these keys end up as diameters in the dictionary, other providers who publish the targeted resource inadvertently picks these bogus keys as part of the anonymization.
\par
On the other hand, such a random placement may seem to aid the dictionary manager in compromising the resource privacy. He may correlate future \textit{(m,n)-} publications with the keys in previous recommendations and make meaningful conclusions. However, keys used in an anonymization i.e., an \textit{(m,n)-entry} are from several different recommendations thanks to the random placement. Hence, the correlations can not differentiate one key from others.
\par
Next, we have to verify that the diameter end points in $D$ are kept intact in $D'$. Here, we prove that the angular distance between two keys in $D$ remains the same in $D'$ and thus does not make previous anonymizations done using $D$ obsolete. This also verifies the previous requirement on the diameter end points as angular distance between these two end points is $180$.
\par
Let us assumes keys $k_1$ and $k_2$ are at an angular distance of $\alpha$ in $D$. Hence,
\begin{equation*}
 Lookup_{D}(k_2) = \left(Lookup_{D}(k_1) + \frac{\alpha}{\left(\frac{360^\circ}{2^h}\right)}\right)\textrm{ mod }2^h 
\end{equation*}
Let this distance be $\alpha'$ in $D'$. Then,
\begin{equation*}
 Lookup_{D'}(k_2) = \left(Lookup_{D'}(k_1) + \frac{\alpha'}{\left(\frac{360^\circ}{2^{h+1}}\right)}\right)\textrm{ mod }2^{h+1} 
\end{equation*}
From eq. (\ref{eq:newindex}), one can see that $\alpha'=\alpha$.
\par
However, it requires a large number of recommendations to fill up the empty slots created as part of dictionary expansion. In practice, additional dictionaries (which are mutually disjoint) smaller in size can be used temporarily to commit the recommendations. These dictionaries must be merged into the main dictionary after sufficient number of keys are accumulated to fill the empty slots created in expansion of the main dictionary. Two equal sized dictionaries can be merged into a bigger dictionary twice in size much similar to the way a dictionary is expanded as explained above. Two dictionaries $D_1$ and $D_2$ of same size can be merged resulting in a dictionary $D$ by interleaving each element of $D_2$ in between two consecutive elements of $D_1$, and as a result,
\begin{equation*}
\begin{split}
\forall k \in D_1, Lookup_{D}(k) = 2 \cdot Lookup_{D_1}(k) \textrm{, and}\\
 \forall k \in D_2, Lookup_{D}(k) = 2 \cdot Lookup_{D_2}(k)+1. 
\end{split}
\end{equation*}
\par
{\bf Upper bound on number of dictionaries:} As mentioned above, multiple dictionaries may exist depending on the frequency and number of input recommendations by the providers. At any instant, the maximum number of such dictionaries $d$ can be $h-1$. 
% \begin{equation*}
% \begin{split}
%  d \leq [\log(\frac{2^h}{m}+1)-1]
% \end{split}
% \end{equation*}
\par
\textit{-\underline{Distribution}}: Initially, a dictionary sufficiently large enough covering resource names from a number of different domains, can be inbuilt into the PANACEA distributions. Once a recommendation is received by the dictionary manager, it waits for a certain time period expecting to accumulate further recommendations and commit all of them to the dictionary. All the recommendations received after this time window are processed similarly. The updated dictionary is lazily pulled by the providers when needed. As long as the resource to be published by the provider is covered by the current version of the dictionary stored on it, no dictionary fetch request is sent to the dictionary manager. If the resource a provider wants to publish is not covered by the dictionary currently stored on the provider, then the latest dictionary is retrieved from the dictionary manager. 
% Any updates on this dictionary later are \textit{pushed} to the providers in the system. Once a recommendation is received by the dicitonary manager, it waits for a certain time period expecting to accumlate further recommendations and commit all of them to the dictionary and pushes the updated dictionary to the providers.
\par
We believe that the proposed simple solution does not result in scalability bottlenecks as dictionary updates will be infrequent after equilibrium is reached. The dictionary manager's unavailability for short durations can be tolerated. However, we acknowledge that finding more sophisticated solution is beyond the scope of the paper. 
\subsection{Discussion}
A resource $r$ with multiple copies is expected to have a larger provider list than that of a resource published by only one provider. In the case of anonymization by RS-approach, the provider list sizes of popular resources (resources with higher no. of providers) tend to be noticeably larger than that of phantom keys as it is highly unlikely that the same phantom keys are chosen in independent random processes. An adversary can exploit this knowledge in order to infer the presence of genuine resources in the system. However, as long as a resource has a provider list size smaller or equal to the maximum possible provider list size for a phantom resource (denoted by $n_f$ in the following sections), an adversary can not differentiate between a genuine and a phantom resource solely based on the provider list sizes. Given this, any RS-mechanism must try to increase the value of $n_f$. We propose an extension to our basic approach that achieves this objective: a peer randomly selects a small partition of the set 
$R$ denoted as $R_L$ ($R_L \subset R$) and constantly employs $R_L$ for the resource anonymization instead of $R$ (referred to as \textit{subset approach}). 
\par
The FS-approach aims to address the possible resource privacy breach for popular resources in the case of random key anonymization. It does so by making the providers choose for the same set of keys as part of anonymization irrespective of the key they want to publish from this set. In other words, the circular approach divides the resource key space into disjoint subsets, each of size $m$. When a provider wants to publish a resource, the entire subset of keys to which the resource's key belongs are selected for publishing as part of anonymization. 
\par
Hence, every key in the subset is as \textit{popular} as any other key in the set, thus phantom keys will also have the provider list size as that of the most popular resource in the system. This is not the case in random resource anonymization, where the provider list sizes of phantom resources can not match some of the most popular resources and thus the popular resources can be easily inferred to exist in the system, thus breaching the resource privacy. 
\par
The FS-approach preserves the privacy of the resources at all levels of popularity. At each level, it blends a resource into a set of $m$ number of resources which share the same level of popularity. In other words, the FS-approach maintains the popularity distribution of genuine and phantom resources in the system.
\par
The FS-approach allows an adversary to construct all the $(m,n)$- entries where a resource appears by simply querying the DHT $m$ number of times. However, this is not always possible because of the overlaps in values which prevent the adversary from constructing exact provider lists for individual \textit{(m,n)-}entries. In the random approach, the adversary can not construct the \textit{(m,n)-}entries since $m-1$ number of keys that could be associated with a key are chosen randomly. However, by looking at the provider lists of DHT entries, the adversary can attempt to construct such entries by picking the keys which have same provider set repeated. But the adversary has to query the system with a very large number of queries. Any case, having the knowledge of multiple \textit{(m,n)}-entries does not breach the privacy as proved in the Theorem. \ref{theorem:privacyforset}.
\par
The RS-approach allows a publisher to multiplex multiple resource publications into a single \textit{(m,n)-}entry. As part of resource anonymization, he can select more than one genuine resource, thus publish multiple resources in a single entry, yet all resources having the same resource privacy. This is not possible in the FS-approach unless the provider owns and wishes to publish other resources corresponding to the entries in the fixed set.
% \par
% However, lets consider the following case in FS-approach. The adversary searches the system for a popular resource and finds an entry in the index, thus constructs the \textit{(m,n)-}entry. Since hash values are chosen arbitrarily in FS-approach, one can assume that an arbitrary hash value may not have a meaningful preimage, and hence, the adversary may discard all the chosen arbitrary hashes in the (m,n)-entry and determine the genuine resource with certainity. But we argue that as long as the preimages of the other hash values are not available (preimage attack is generally considered to be infeasible), the adversary's action of discarding other hashes can be wrong with non-negligible probability. Each of the other hashes, equally likely, might correspond to a genuine resource.
% \par
% In FS-approach, lets consider that an adversary sees $m\cdot n$ number of \textit{(m,n)-}entries with the same key and value set in all the entries. Such an adversary will be most likely part of this set of providers as an arbitrary peer in the network may not get a chance to observe all of such similar entries. Since typically a provider does not publish the same resource second time, this discloses a resource presence and its providers. All the resources appeared in the entries are present with each of the providers mentioned. However, the possiblity of such a case is extremely rare as it is highly unlikely that the same providers possessing the resources appearing at arbitrary points in a hash space. Moreover, as part of the randomized routing all peers in an $(m,n)$- entry may not be visited during its insertion into the DHT. Hence, we assume that this case does not hinder the relevance of the PANACEA approach to the problem of preserving privacy of access controlled data in P2P systems. Such a case is 
even more unlikely to occur with RS-approach because of random selection of anonymous keys.
\par
In the rest of the paper, we assume resource anonymization using FS-approach unless mentioned otherwise. 
% \begin{itemize} 
% \item Genuine resources can be mixed in one publication.
% \item Since a meaningful name mapped to a key is rare, can someone assume all remaining (m-1) entries bogus? It does not happen in random approach.
% \end{itemize}
% \subsection{Notes}
% \begin{itemize}
%  \item Since a meaningful name mapped to a key is rare, can someone assume all remaining (m-1) entries bogus?
% \item Attacker can construct all (m,n) keys easily..
% \item There is already resource privacy available with hashing itself: since we take higher-order $h$ bits of a hashed value for a $2^h$ sized key space, 
% \end{itemize}
%\input{fsapproach-consistenthash.tex}