(* Content-type: application/mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 7.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       145,          7]
NotebookDataLength[      5535,        158]
NotebookOptionsPosition[      5037,        138]
NotebookOutlinePosition[      5474,        155]
CellTagsIndexPosition[      5431,        152]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell[BoxData[{
 RowBox[{
  RowBox[{"Clear", "[", 
   RowBox[{"R", ",", "r", ",", "m"}], "]"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"R", "=", " ", 
   SuperscriptBox["26", "10"]}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"r", " ", "=", " ", "1000000"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"m", " ", "=", "5"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"mu", " ", "=", " ", "0.6"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"$MaxExtraPrecision", "=", "1"}], ";"}], "\[IndentingNewLine]", 
  RowBox[{"(*", 
   RowBox[{
    RowBox[{
     RowBox[{"XXX", "[", "RRR_", "]"}], " ", ":=", " ", 
     RowBox[{
      RowBox[{"[", 
       RowBox[{"1", "-", 
        SuperscriptBox[
         RowBox[{"(", 
          RowBox[{"1", "-", 
           RowBox[{"(", 
            RowBox[{"1", "/", "RRR"}], ")"}]}], ")"}], 
         RowBox[{
          RowBox[{"r", " ", "m"}], " ", "-", "r"}]]}], "]"}], " ", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{"m", "-", "1"}], ")"}], "/", "m"}]}]}], ";"}], 
   "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{
    RowBox[{"XXX", "[", "RRR_", "]"}], " ", ":=", " ", 
    RowBox[{"1", "-", 
     SuperscriptBox[
      RowBox[{"(", 
       RowBox[{"1", "-", 
        RowBox[{"(", 
         RowBox[{"1", "/", "RRR"}], ")"}]}], ")"}], 
      RowBox[{
       RowBox[{"r", " ", "m"}], " ", "-", "r"}]]}]}], " ", ";"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{" ", 
  RowBox[{
   RowBox[{
    ButtonBox["FractionalPart",
     BaseStyle->"Link",
     ButtonData->"paclet:ref/FractionalPart"], "[", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{"1", "-", 
       SuperscriptBox[
        RowBox[{"(", 
         RowBox[{"1", "-", 
          RowBox[{"(", 
           RowBox[{"1", "/", "R"}], ")"}]}], ")"}], 
        RowBox[{"mu", 
         RowBox[{"(", 
          RowBox[{
           RowBox[{"r", " ", "m"}], " ", "-", "r"}], ")"}]}]]}], ")"}], 
     RowBox[{
      RowBox[{"(", " ", 
       RowBox[{"m", "-", "1"}], ")"}], "/", "m"}]}], "]"}], ";"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"N", "[", "%", "]"}], 
  "\[IndentingNewLine]"}], "\[IndentingNewLine]", 
 RowBox[{" "}]}], "Input",
 CellChangeTimes->{{3.467995005307761*^9, 3.467995119916587*^9}, 
   3.467995184755646*^9, {3.467995278156396*^9, 3.467995293558073*^9}, {
   3.467995346283335*^9, 3.467995406316156*^9}, {3.46799548947192*^9, 
   3.467995495413596*^9}, {3.467995539009096*^9, 3.467995616489199*^9}, {
   3.46799571737219*^9, 3.467995756130238*^9}, {3.467995874491644*^9, 
   3.467995929303898*^9}, {3.467995985309244*^9, 3.467996000825954*^9}, {
   3.467996066664877*^9, 3.467996107027875*^9}, {3.467996142418686*^9, 
   3.467996206234039*^9}, {3.467996385518961*^9, 3.467996476072391*^9}, {
   3.467996523966737*^9, 3.467996604190447*^9}, {3.467996657154315*^9, 
   3.467996732869961*^9}, {3.467996810578355*^9, 3.467996816694721*^9}, 
   3.467996874915748*^9, {3.468170319799474*^9, 3.468170396224126*^9}, {
   3.468170427544054*^9, 3.468170444248375*^9}, {3.468170511432185*^9, 
   3.468170524007921*^9}, {3.468170584108513*^9, 3.468170633890417*^9}}],

Cell[BoxData[
 RowBox[{
  ButtonBox["FractionalPart",
   Appearance->Automatic,
   BaseStyle->"Link",
   ButtonData->"paclet:ref/FractionalPart",
   Evaluator->Automatic,
   Method->"Preemptive"], "[", "1.3642420437776082`*^-8", "]"}]], "Output",
 CellChangeTimes->{{3.467995909358053*^9, 3.467995937853556*^9}, {
   3.467995989070829*^9, 3.467996002507072*^9}, {3.467996068722296*^9, 
   3.467996108764239*^9}, {3.4679961521299*^9, 3.467996208000466*^9}, {
   3.467996393009689*^9, 3.467996476802987*^9}, {3.467996539137997*^9, 
   3.467996605480604*^9}, {3.467996718596455*^9, 3.467996734662608*^9}, 
   3.467996818156152*^9, {3.468170384712302*^9, 3.468170400374159*^9}, 
   3.468170446315466*^9, 3.468170548227019*^9, 3.468170638415402*^9}]
}, Open  ]],

Cell[BoxData[
 RowBox[{"\[IndentingNewLine]", "\[IndentingNewLine]", 
  "\[IndentingNewLine]"}]], "Input",
 CellChangeTimes->{{3.467995721670351*^9, 3.467995724875767*^9}}],

Cell[BoxData[""], "Input",
 CellChangeTimes->{{3.467995174420972*^9, 3.467995241759884*^9}, {
  3.467995276382971*^9, 3.467995282552475*^9}, {3.467995353227688*^9, 
  3.467995357270287*^9}, {3.467995390543259*^9, 3.467995417721826*^9}, {
  3.467995646758752*^9, 3.467995650197147*^9}, {3.467995686695931*^9, 
  3.467995720009793*^9}}]
},
WindowSize->{1600, 1128},
WindowMargins->{{3, Automatic}, {25, Automatic}},
Magnification:>FEPrivate`If[
  FEPrivate`Equal[FEPrivate`$VersionNumber, 6.], 1.5, 1.5 Inherited],
FrontEndVersion->"7.0 for Linux x86 (32-bit) (February 25, 2009)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[567, 22, 3195, 85, 441, "Input"],
Cell[3765, 109, 744, 14, 59, "Output"]
}, Open  ]],
Cell[4524, 126, 172, 3, 137, "Input"],
Cell[4699, 131, 334, 5, 46, "Input"]
}
]
*)

(* End of internal cache information *)
