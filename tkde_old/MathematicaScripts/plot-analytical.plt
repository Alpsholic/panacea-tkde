set autoscale x

#set xrange [1208362379:1208550000]
#set datafile separator ","

#set key bottom right

#set key autotitle columnhead
set size 0.7,0.7
#set key graph 0.9, graph 0.8
#set format x "%.5t^%T"

set terminal postscript enhanced
set xlabel '{/Symbol m}'

#set output 

#set output "auth-nanc-costxx.eps"
#plot "errror6.txt" using 1:2:3 title 'PANACEA-Simulations' with yerrorbars

set ylabel '{C_{s,a}}'
set output "auth-nanc-cost.eps"
plot "analytical.txt" using 1:10 title 'PANACEA-Analytical' with linespoints,\
"analytical.txt" using 1:12 title 'PPI-Analytical' with linespoints

set ylabel '{P_{K,a}}'
set output "auth-nanc-K.eps"
plot "analytical.txt" using 1:2 title 'PANACEA-Analytical' with linespoints,\
"analytical.txt" using 1:4 title 'PPI-Analytical' with linespoints



set ylabel '{P_{V,a}}'
set output "auth-nanc-V.eps"
plot "analytical.txt" using 1:6 title 'PANACEA-Analytical' with linespoints,\
"analytical.txt" using 1:8 title 'PPI-Analytical' with linespoints


set ylabel '{P_{K,u}}'
set output "unauth-nanc-K.eps"
plot "analytical.txt" using 1:3 title 'PANACEA-Analytical' with linespoints,\
"analytical.txt" using 1:5 title 'PPI-Analytical' with linespoints


set ylabel '{P_{V,u}}'
set output "unauth-nanc-V.eps"
plot "analytical.txt" using 1:7 title 'PANACEA-Analytical' with linespoints,\
"analytical.txt" using 1:9 title 'PPI-Analytical' with linespoints


#set key right center 
set key 100,100
set ylabel '{C_{s,u}}'
set output "unauth-nanc-cost.eps"
plot "analytical.txt" using 1:11 title 'PANACEA-Analytical' with linespoints,\
"analytical.txt" using 1:13 title 'PPI-Analytical' with linespoints


#set output "wan-micro-model.eps"
#plot "wan-micro-model.csv" using 1:2 title 'Real' with linespoints,\
#     "wan-micro-model.csv" using 3:4 title 'LR-SMA(5,7)' with linespoints
