\section{Analysis}
\label{sec:analysis}
In this section, we analytically study the privacy offered by PANACEA by employing probability theory and information theory approaches. Moreover, we estimate the expected communication overhead of our approach.

We use the following notation in our analysis. Let $N$ be the number of peers in the system and $N_c$ be the expected number of copies of a genuine resource $r$ and $N_a\le N_c$ be the number of copies that a particular user is authorized to access. We call a user as \textit{unauthorized} to $r$, when he is not authorized to access any of the $N_c$ copies of $r$, i.e. $N_a=0$. 
%We compare the properties of our approach with those of the extreme cases of a completely unstructured (e.g. Gnutella) and of a fully structured P2P system (e.g. Chord) that offer access control. 
\subsection{Probabilistic approach}
We evaluate the privacy breach that can be achieved by a user who has {\em complete} knowledge on the parameters of our PANACEA system and queries the system for a particular resource. %\textbf{Other cases where users have limited or no knowledge of the systems and case of attacks employing multiple queries to violate privacy of PANACEA, are discussed in Section \ref{sec:attacks}.}
We denote:
\begin{itemize} 
\item[i)] $P_{K,a}$ (resp. $P_{K,u}$) as the probability for an authorized (resp. unauthorized) user to deduce the existence of a certain genuine resource.
\item[ii)] $P_{V,a}$ (resp. $P_{V,u}$) as the probability for an authorized (resp. unauthorized) user to deduce the provider of a certain genuine resource.
\item[iii)] $P_{-}$ as the probability for an authorized or unauthorized user to deduce the non-existence of a certain {\em non-existing} resource.
\end{itemize}
\begin{definition}
An access-controlled system is said to provide \textit{higher} privacy if it promises: 
\begin{itemize}
\item[i)] Lower probability for an unauthorized user to deduce a resource's presence and its provider ($P_{K,u}$, $P_{V,u}$)
\item[ii)] Lower probability for a user deducing a resource's non-existence ($P_{-}$)
\end{itemize}
\label{def}
\end{definition}
Under this definition of privacy, any \textit{privacy-efficient} access control mechanism should aim to:
\begin{itemize}
\item Minimize $P_{K,u}$, $P_{V,u}$, $P_{-}$, which should ideally be 0 as in unstructured P2P systems.
\item Maximize search cost $C_{s,u}$ for unauthorized users and ideally close to that of the unstructured P2P systems.
\end{itemize}
However, the \textit{search efficiency} of the privacy-enabling mechanism should remain high, i.e.:
\begin{itemize}
\item $P_{K,a}$, $P_{V,a}$ should ideally be 1 (as in structured P2P systems), and
\item The search communication cost $C_{s,a}$ should be kept low and ideally close to that of the structured P2P systems.
\end{itemize} 
We express the privacy and search cost metrics of PANACEA in terms of the corresponding metrics of structured and unstructured P2P systems. To this end, we use superscripts $U$ and $S$ to denote metrics for unstructured and structured P2P systems respectively, and no superscript for PANACEA, e.g. $P_{K,u}^{U}$ refers to unstructured systems and the equivalent metric for PANACEA is $P_{K,u}$.

First, we quantify provider privacy for an authorized user. There are three cases that can arise:

\textit{Case (i):} If any of the $N_a$ copies, where he is authorized to, was published to the DHT, he could deduce the provider of the resource with probability $1$. The probability that at least one of $N_a$ copies was published into the DHT is $1-(1-\mu)^{N_a}$.

\textit{Case (ii):} On the other hand, consider the case that none of the $N_a$ copies was published in the DHT (probability of which, is $(1-\mu)^{N_a}$), but some $i$ of the remaining $N_c-N_a$ copies were published. Recall that if the user finds an $(m,n)$-entry for $r$ in the DHT, he contacts all the providers therein. In this case, as an authorized copy of the resource would not be located by contacting $i\cdot n$ number of providers listed in the DHT entry, the user would also employ flooding. Here, we say that the user could deduce the provider with probability $\frac{1}{i\cdot n}$ in addition to what is possible to be deduced by flooding (i.e., $P_{V,a}^U$). However, the number of providers is less than $i\cdot n$, because of ``collisions'' (i.e. provider id conflicts) in the provider lists across the multiple $(m,n)$ entries of the resource copies. We account for this with a \textit{collision factor} $f_v$.

\textit{Case (iii):} When a DHT entry is not found for the resource key (probability of which is $(1-\mu)^{N_c}$)\footnote{In fact, a DHT entry can also be present for a key being a phantom one. However, we assume this probability as negligible, as $|R|$ is big compared to the number of genuine resources.}, the user attempts to deduce the provider by flooding ($P_{V,a}^U$). Hence,
\begin{equation}
\begin{split}
P_{V,a} = & \left[1-(1-\mu)^{N_a}\right] \cdot 1  +  \\
& (1-\mu)^{N_a} \cdot \sum_{i=1}^{N_c-N_a}{{N_c-N_a}\choose{i}}\mu^i (1-\mu)^{(N_c-N_a-i)}\\
&\cdot \left[\frac{1}{(i\cdot n)(1-f_v)} + P_{V,a}^U\right]+(1-\mu)^{N_c}\cdot P_{V,a}^U\>.
\end{split}
\label{pva_panacea}
\end{equation}
We apply similar reasoning to formulate resource privacy $P_{K,a}$ for an authorized user in addition to the following. Let the maximum provider list size $n_f$ of a phantom resource key be known to the user. Then, if the size of provider list of the $(m,n)$-entry of the queried resource is larger than $n_f$, the user can deduce the existence of the resource in the system with probability $1$. Otherwise if it is smaller than $n_f$, a probability $\frac{1}{m}$ can be assigned to the existence, as the queried key is mixed with $m-1$ other ones in the $(m,n)$-entry, in addition to what can also be assigned by flooding ($P_{K,a}^U$). Therefore,
\begin{equation}
\begin{split}
P_{K,a}& =  \left[1-(1-\mu)^{N_a}\right] \cdot 1  + (1-\mu)^{N_a} \cdot (1-(1-\mu)^{N_c-N_a}) \\
& \mathbf{I}\left(\sum_{i=1}^{N_c-N_a}{{N_c-N_a}\choose{i}}\mu^i (1-\mu)^{(N_c-N_a-i)}(in)(1-f_v)\right)\\
& +(1-\mu)^{N_c}\cdot P_{K,a}^U\textrm{, where}\\
\mathbf{I}(l)& = \left\{\begin{array}{cl} 1\textrm{,} ~~~\textrm{for } l\geq n_f \\ P_{K,a}^U \cdot1 + (1-P_{K,a}^U)\cdot \frac{1}{m} \textrm{, otherwise} \\ \end{array} \right\}\>.
\end{split}
\label{pka_panacea}
\end{equation}
Next, we quantify the search cost $C_{s,a}$ in terms of the number of messages generated because of a query from an authorized user. First, a user searches in the DHT which incurs a cost of $C_{s,a}^S$. Thereafter, we account for two possible cases- none of $N_c$ copies or some $i$ copies of the resource are published into the DHT. The former case can happen with probability $(1-\mu)^{N_c}$ where the user employs flooding, incurring a cost of $C_{s,a}^U$. In the latter case, $i\cdot n$ number of providers are contacted. If none of them has an authorized copy (probability of which, is $(1-\frac{N_a}{N_c})^{i}$), the user employs flooding. Overall, $C_{s,a}$:
\begin{equation}
\begin{split}
% C_{p,a} & = \sum_{i=1}^{N_c}{{N_c}\choose{i}}\mu^i(1-\mu)^{(N_c-i)}\cdot i\cdot \left(\frac{1}{\lambda}+ m \cdot C_{p,a}^S\right) \\
C_{s,a} & = C_{s,a}^S +(1-\mu)^{N_c}\cdot C_{s,a}^U+ \sum_{i=1}^{N_c}{{N_c}\choose{i}}\mu^i(1-\mu)^{(N_c-i)}\\
&\cdot\left[i\cdot n\cdot(1-f_v)+ \left(1-\frac{N_a}{N_c}\right)^{i} C_{s,a}^U\right]
\end{split}
\label{csa_panacea}
\end{equation}
The privacies $P_{K,u}$, $P_{V,u}$, and the search cost $C_{s,u}$ for an unauthorized user are given by eq. (\ref{pva_panacea}) to eq. (\ref{csa_panacea}) for $Na=0$.

Finally, we derive $P_{-}$, i.e. the probability to deduce the non-existence of a non-existing resource. Given an event space $\Omega=\{$DHT,$\neg$DHT$\}$ that a non-existent resource is found or not in the DHT respectively, $P_{-}$ is given by:

\small
\begin{equation}
\begin{split}
P_{-} = Pr(-\mid\Omega) =  & Pr(-\mid \neg \textrm{DHT})\cdot Pr(\neg \textrm{DHT})+\\
& Pr(-\mid \textrm{DHT})\cdot Pr(\textrm{DHT})\>,\textrm{where}\\
%Pr[\textrm{Not Find}]& =  1-\left[\mu\cdot\frac{1}{m}+(1-\mu)P_{K,u}^U\right] \\
Pr(-\mid \neg \textrm{DHT}) =  & P_{K,u}^U = 0\\
Pr(-\mid \textrm{DHT}) = & \frac{m-1}{m} \\
Pr(\textrm{DHT}) = & \left[1-\left(1-\frac{1}{\mid R\mid}\right)^{\mu N_r (m-1)}\right] \\
Pr(\neg \textrm{DHT}) = & 1 - Pr(\textrm{DHT})
\end{split}
\label{pminus_panacea}
\end{equation}
\normalsize
$N_r$ is the total number of resources in the system and $R$ is the resource namespace. $Pr(-\mid \neg \textrm{DHT})$ expresses the probability that a resource is non-existent, given that it is not found in the DHT. This is similar to the probability of deducing the existence of an unauthorized resource for a user in unstructured P2P systems, because an existing resource is same as a non-existing resource for an unauthorized user. $Pr(-\mid \textrm{DHT})$ is the probability that the resource corresponding to the key does not exist. $Pr(\textrm{DHT})$ expresses the probability that a phantom resource from namespace $R$ may have been inserted into the index, while $Pr(\neg \textrm{DHT})$ is the complement of $Pr(\textrm{DHT})$. Observe that $P_{-}$ is minimal ($\sim 0$) for reasonable values of the various parameters. Also, we estimate the expected query cost to deduce the non-existence. The user first searches for an index entry and then employs flooding, hence,
\begin{equation}
C_{s,-} = C_{s,-}^S + C_{s,-}^U\>.
\label{csminus_panacea}
\end{equation}
\subsection{Information-theoretic approach}
In \cite{serjantov02towards,Diaz02towardsmeasuring}, an information theoretic approach was proposed to measure privacy offered by a system  employing {\em entropy} $H$ as an anonymity metric, which is defined as:
\begin{equation}
H=-\displaystyle \sum_i p_i \log_2 p_i\>,
\end{equation}
where $p_i$ is the attacker's estimate of the probability that a participant $i$ was responsible for some observed action. Entropy is
maximized to $\log_2 |A|$ if equal probability is assigned to all members of the anonymity set $A$, and it is minimized at 0 when $|A|=1$.
According to \cite{serjantov02towards}, a system with entropy $H$ has {\em effective anonymity set} of size $2^H$.
As an adversary in PANACEA may have different information sets (i.e. each resulting from different observations), {\em conditional entropy} $H_0$ \cite{Diaz02towardsmeasuring} is a more appropriate metric, which is given by:
\begin{equation}
H_0=\displaystyle \sum_{y}Pr[Y=y]H(X|Y=y)=E_y H(X|Y=y)\>,
\end{equation}
where $X$ is a random variable of the private aspect to be preserved and $Y$ models the different observations $y$.

Next, we calculate the entropy of PANACEA for resource anonymity against a searcher. To this end, the random variable $X$ models the existence of a resource, i.e. whether a resource name from the resource namespace $R$ exists in the system or not. The random variable $Y$ models the observations of the searcher for a requested resource: (i) an authorized copy is found in the DHT, (ii) an unauthorized copy is found in the DHT and an authorized copy is found by flooding, (iii) an unauthorized copy is found in the DHT but no authorized copy is found by flooding, (iv) no copy is found in the DHT and an authorized copy was found by flooding, and (v) no copy is found in the DHT but no authorized copy was found by flooding.
In the information sets (i) and (ii), there is no anonymity, thus entropy is 0. In the case of (iii), if the expected provider list size of the queried resource is greater than $n_f$, then there is no privacy and the resource is existent. Otherwise, the probability assigned to any resource in the $(m,n)$-entry to be existent is $1/m$, because this key was inserted with a mix of $m$ keys. Thus, the anonymity set size is $m$ in that case. In the observation (iv), the anonymity set is 1, as the queried resource is found. Finally, in the case of (v), the anonymity set is $R$, as the queried resource is not found and it can be any of the resources in the resource namespace $R$. Therefore, the resource entropy $H_{k}$ for a searcher is given by:
\begin{equation}
\begin{split}
H_{K}& = E_{\textrm{iii}}H(X|Y=\textrm{iii})+E_{\textrm{v}}H(X|Y=\textrm{v}) \\
&= -(1-\mu)^{N_a}(1-(1-\mu)^{N_c-N_a})(1-P_{K,a}^U)\\
& \mathbf{1}(E(n')<n_f)\log_2{\frac{1}{m}}-(1-\mu)^{N_c}(1-P_{K,a}^U)\log_2{\frac{1}{|R|}}\>,
\end{split}
\label{r_entropy}
\end{equation}
where $E(n')$ is the expected provider list size in the $(m,n)$-entry for the queried resource and $n_f$ is the maximum provider list size for a phantom resource respectively. $E(n') = \sum_{i=1}^{N_c-N_a}{{N_c-N_a}\choose{i}}\mu^i (1-\mu)^{(N_c-N_a-i)}(in)(1-f_v)$ and $\mathbf{1}()$ is an indicator function.

Also, regarding provider anonymity, the random variable $X$ models the publisher of the requested resource and the random variable $Y$ models the information sets for the searcher as in the case of resource anonymity.
If the searcher has made the observations (i), (ii), then the provider entropy is 0. In the case (iii), where an entry for the requested resource is found in the DHT, but not for an authorized copy, and no authorized copy was found by flooding, we calculate $H(X|Y=iii)$ according to the following logic: If the expected number of providers for the requested resource as in the DHT is $E(n')$, then the publisher of the requested resource can be either one of these entries in the provider list or one peer in rest of the system. Therefore, each peer in the provider list has probability $1/(E(n')+1)$ to be the publisher and any other peer has probability $1/(E(n')+1)(N-E(n'))$ to be the publisher of the requested resource. If the observation (iv) is made by the searcher, then the publisher is located and thus the entropy is 0. Finally, in the case of observation (v), the anonymity set is the complete set $N$ of peers in the system, as the publisher is not located. 
Therefore, the provider entropy $H_{V}$ is:

\small
\begin{equation}
\begin{split}
H_{V} = & E_{\textrm{iii}}H(X|Y=\textrm{iii})+E_{\textrm{v}}H(X|Y=\textrm{v}) \\
= & -(1-\mu)^{N_a}(1-(1-\mu)^{N_c-N_a})(1-P_{K,a}^U)\\ 
& (E(n')\frac{1}{E(n')+1}\log_2{\frac{1}{E(n')+1}}+\\
& (N-E(n'))\frac{1}{(N-E(n'))(E(n')+1)}\\
& \log_2{\frac{1}{(N-E(n'))(E(n')+1)}}\\
& -(1-\mu)^{N_c}(1-P_{K,a}^U)\log_2{\frac{1}{N}}
\end{split}
\label{p_entropy}
\end{equation}
\normalsize
For a searcher authorized to a copy of the queried resource, the resource and provider entropies are given by eq. (\ref{r_entropy}), (\ref{p_entropy}) respectively. On the other hand, for an unauthorized searcher, they are given by eq. (\ref{r_entropy}), (\ref{p_entropy}) respectively for $N_a=0$.