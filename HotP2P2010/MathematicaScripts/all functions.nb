(* Content-type: application/mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 7.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       145,          7]
NotebookDataLength[     10193,        289]
NotebookOptionsPosition[      9845,        273]
NotebookOutlinePosition[     10203,        289]
CellTagsIndexPosition[     10160,        286]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{
Cell[BoxData[
 RowBox[{
  StyleBox[" ",
   Background->RGBColor[0.87, 0.94, 1]], 
  RowBox[{
   StyleBox[
    RowBox[{
     RowBox[{
      SubscriptBox["PaV", "CAC"], "=", "1"}], ";"}],
    Background->RGBColor[0.87, 0.94, 1]], "\[IndentingNewLine]", 
   StyleBox[
    RowBox[{
     RowBox[{
      SubscriptBox["PaMinus", "CAC"], "=", " ", "1"}], ";", " ", 
     RowBox[{
      SubscriptBox["Cp", "CAC"], "[", 
      RowBox[{"NNN_", ",", "Ncc_"}], "]"}]}],
    Background->RGBColor[0.87, 0.94, 1]], "\[IndentingNewLine]", 
   StyleBox[" ",
    Background->RGBColor[0.87, 0.94, 1]], 
   StyleBox[
    RowBox[{
     RowBox[{
      SubscriptBox["Cs", 
       RowBox[{"CAC", " "}]], "[", 
      RowBox[{"NNN_", ",", "Ncc_"}], "]"}], "  ", ";", " ", 
     RowBox[{
      SubscriptBox["Cta", 
       RowBox[{"CAC", " "}]], "[", 
      RowBox[{"NNN_", ",", " ", "Ncc_"}], "]"}]}],
    Background->RGBColor[0.87, 0.94, 1]], "\[IndentingNewLine]", 
   "\[IndentingNewLine]", "\[IndentingNewLine]", 
   StyleBox[
    RowBox[{
     RowBox[{
      SubscriptBox["PuMinus", "CAC"], " ", "=", " ", "1"}], ";"}],
    Background->RGBColor[0.87, 0.94, 1]], 
   StyleBox["\[IndentingNewLine]",
    Background->RGBColor[0.87, 0.94, 1]], 
   StyleBox[
    RowBox[{
     SubscriptBox["Ctu", "CAC"], "[", 
     RowBox[{"NNN_", ",", "Ncc_"}], "]"}],
    Background->RGBColor[0.87, 0.94, 1]], "\[IndentingNewLine]", 
   "\[IndentingNewLine]", 
   RowBox[{"alpha", " ", "=", " ", 
    RowBox[{"Function", "[", 
     RowBox[{"{", 
      RowBox[{"tttl", ",", 
       RowBox[{
        RowBox[{
         RowBox[{"pp", "\[IndentingNewLine]", 
          SubscriptBox["PaK", "CAC"]}], "=", "1"}], ";", "  ", 
        StyleBox[
         RowBox[{
          SubscriptBox["PaK", "GAC"], "[", 
          RowBox[{"alph_", ",", "pp_"}], "]"}],
         Background->RGBColor[1, 0.9, 0.8]], 
        StyleBox["  ",
         Background->RGBColor[1, 0.9, 0.8]], 
        StyleBox[";",
         Background->RGBColor[1, 0.9, 0.8]], 
        StyleBox[" ",
         Background->RGBColor[1, 0.9, 0.8]], 
        RowBox[{
         RowBox[{
          SubscriptBox["PaK", "HAC"], "[", 
          RowBox[{
          "miu_", ",", "Naa_", ",", " ", "Ncc_", ",", " ", "mm_", ",", " ", 
           "alph_", ",", " ", "pp_"}], "]"}], "\[IndentingNewLine]", 
         StyleBox[
          RowBox[{
           SubscriptBox["PaV", "GAC"], " ", "[", 
           RowBox[{"alph_", ",", " ", "pp_"}], "]"}],
          Background->RGBColor[1, 0.9, 0.8]]}], 
        StyleBox["  ",
         Background->RGBColor[1, 0.9, 0.8]], 
        StyleBox[";",
         Background->RGBColor[1, 0.9, 0.8]], 
        RowBox[{
         SubscriptBox["PaV", "HAC"], "[", 
         RowBox[{
         "miu_", ",", "Naa_", ",", " ", "Ncc_", ",", " ", "mm_", ",", " ", 
          "alph_", ",", " ", "nn_", ",", "pp_"}], "]"}], 
        StyleBox["   ",
         Background->RGBColor[1, 0.9, 0.8]], 
        StyleBox[";",
         Background->RGBColor[1, 0.9, 0.8]], 
        StyleBox[
         RowBox[{
          SubscriptBox["PaV", "CAC"], "=", "1"}],
         Background->RGBColor[0.87, 0.94, 1]], 
        StyleBox[";",
         Background->RGBColor[0.87, 0.94, 1]], 
        StyleBox["               ",
         Background->RGBColor[1, 0.9, 0.8]], 
        StyleBox["\[IndentingNewLine]",
         Background->RGBColor[1, 0.9, 0.8]], 
        StyleBox["\[IndentingNewLine]",
         Background->RGBColor[1, 0.9, 0.8]], 
        StyleBox[
         RowBox[{
          SubscriptBox["PuK", "GAC"], "=", "0"}],
         Background->RGBColor[1, 0.9, 0.8]], 
        StyleBox[";",
         Background->RGBColor[1, 0.9, 0.8]], 
        StyleBox[" ",
         Background->RGBColor[1, 0.9, 0.8]], 
        RowBox[{
         SubscriptBox["PuK", "HAC"], "[", 
         RowBox[{"Ncc_", ",", "muu_", ",", " ", "mm_"}], "]"}], ";", " ", 
        StyleBox[
         RowBox[{
          SubscriptBox["PuK", "CAC"], "=", "1"}],
         Background->RGBColor[0.87, 0.94, 1]], 
        StyleBox[";",
         Background->RGBColor[0.87, 0.94, 1]], 
        StyleBox["\[IndentingNewLine]",
         Background->RGBColor[0.87, 0.94, 1]], 
        StyleBox[
         RowBox[{
          SubscriptBox["PuV", "GAC"], "=", "0"}],
         Background->RGBColor[1, 0.9, 0.8]], 
        StyleBox[";",
         Background->RGBColor[1, 0.9, 0.8]], 
        StyleBox[" ",
         Background->RGBColor[1, 0.9, 0.8]], 
        RowBox[{
         SubscriptBox["PuV", "HAC"], "[", 
         RowBox[{"Ncc_", ",", "muu_", ",", "nn_"}], "]"}], " ", ";", 
        StyleBox[
         RowBox[{
          SubscriptBox["PuV", "CAC"], "=", "1"}],
         Background->RGBColor[0.87, 0.94, 1]], 
        StyleBox[";",
         Background->RGBColor[0.87, 0.94, 1]], 
        StyleBox["\[IndentingNewLine]",
         Background->RGBColor[0.87, 0.94, 1]], 
        StyleBox["\[IndentingNewLine]",
         Background->RGBColor[1, 0.9, 0.8]], 
        RowBox[{
         RowBox[{
          StyleBox[
           RowBox[{
            SubscriptBox["Cta", "GAC"], "[", "alph_", "]"}],
           Background->RGBColor[1, 0.9, 0.8]], "\[IndentingNewLine]", 
          StyleBox[
           RowBox[{
            SubscriptBox["Cs", "GAC"], "[", "alph_", "]"}],
           Background->RGBColor[1, 0.9, 0.8]], 
          StyleBox["  ",
           Background->RGBColor[1, 0.9, 0.8]], 
          StyleBox[
           SubscriptBox["Cp", "GAC"],
           Background->RGBColor[1, 0.9, 0.8]]}], 
         StyleBox["=",
          Background->RGBColor[1, 0.9, 0.8]], 
         StyleBox["0",
          Background->RGBColor[1, 0.9, 0.8]]}], 
        StyleBox[";",
         Background->RGBColor[1, 0.9, 0.8]], "\[IndentingNewLine]", 
        "\[IndentingNewLine]", 
        StyleBox[
         RowBox[{
          SubscriptBox["PaMinus", 
           RowBox[{"GAC", " "}]], "=", " ", "0"}],
         Background->RGBColor[1, 0.9, 0.8]], 
        StyleBox[";",
         Background->RGBColor[1, 0.9, 0.8]], 
        RowBox[{
         SubscriptBox["PaMinus", "HAC"], "[", 
         RowBox[{"miu_", ",", " ", "Ncc_"}], "]"}], ";", 
        RowBox[{
         SubscriptBox["PaMinus", "CAC"], "=", " ", "1"}], ";", 
        "\[IndentingNewLine]", 
        StyleBox[
         RowBox[{
          SubscriptBox["CtaMinus", "GAC"], "=", "alpha"}],
         Background->RGBColor[1, 0.9, 0.8]], 
        StyleBox[";",
         Background->RGBColor[1, 0.9, 0.8]], "\[IndentingNewLine]", 
        "\[IndentingNewLine]", 
        RowBox[{
         RowBox[{
          StyleBox[
           RowBox[{
            SubscriptBox["Ctu", "GAC"], "[", "alph_", "]"}],
           Background->RGBColor[1, 0.9, 0.8]], "\[IndentingNewLine]", 
          StyleBox[
           SubscriptBox["PuMinus", "GAC"],
           Background->RGBColor[1, 0.9, 0.8]]}], 
         StyleBox["=",
          Background->RGBColor[1, 0.9, 0.8]], 
         StyleBox["0",
          Background->RGBColor[1, 0.9, 0.8]]}], 
        StyleBox[";",
         Background->RGBColor[1, 0.9, 0.8]], "\[IndentingNewLine]", 
        RowBox[{
         StyleBox[
          RowBox[{
           SubscriptBox["CtuMinus", 
            RowBox[{"GAC", " "}]], "[", "alph_", "]"}],
          Background->RGBColor[1, 0.9, 0.8]], "\[IndentingNewLine]", 
         "\[IndentingNewLine]", "\[IndentingNewLine]", 
         RowBox[{
          SubscriptBox["Cp", "HAC"], "[", 
          RowBox[{
          "miu_", ",", " ", "Ncc_", ",", " ", "mm_", ",", " ", "alph_", ",", 
           "NNN_"}], "]"}], "\[IndentingNewLine]", 
         RowBox[{
          SubscriptBox["Cs", "HAC"], "[", 
          RowBox[{
          "miu_", ",", " ", "Naa_", ",", " ", "Ncc_", ",", " ", "alph_", ",", 
           " ", "nn_", ",", " ", "NNN_"}], "]"}], "\[IndentingNewLine]", 
         RowBox[{
          SubscriptBox["Cta", "HAC"], "[", 
          RowBox[{
          "miu_", ",", " ", "Naa_", ",", "Ncc_", ",", " ", "mm_", ",", " ", 
           "alph_", ",", " ", "nn_", ",", "NNN_"}], "]"}], " ", 
         "\[IndentingNewLine]"}]}]}]}]}]}]}]}]], "Input",
 CellChangeTimes->{{3.46713129306996*^9, 3.46713133038915*^9}, {
  3.46713145271479*^9, 3.467131581598604*^9}, {3.467131646192372*^9, 
  3.467131673305944*^9}, {3.467187249342237*^9, 3.467187275807207*^9}, {
  3.467188981740523*^9, 3.467189032047551*^9}, {3.467189157259062*^9, 
  3.467189159568026*^9}, {3.46721237969537*^9, 3.467212400067824*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{
   SubscriptBox["CsaMinus", "HAC"], "[", 
   RowBox[{"NNN_", ",", " ", "alph_"}], "]"}], "     "}]], "Input",
 CellChangeTimes->{{3.467131537333099*^9, 3.467131549430106*^9}}],

Cell[BoxData[
 RowBox[{" ", "\[IndentingNewLine]", "\[IndentingNewLine]", 
  RowBox[{
   RowBox[{
    SubscriptBox["Csu", "HAC"], "[", 
    RowBox[{
    "NNN_", ",", " ", "Ncc_", ",", " ", "muu_", ",", " ", "nn_", ",", " ", 
     "alph_"}], "]"}], " ", "\[IndentingNewLine]", 
   RowBox[{
    SubscriptBox["PuMinus", "HAC"], "[", 
    RowBox[{"miu_", ",", " ", "Ncc_"}], "]"}], " ", "\[IndentingNewLine]", 
   RowBox[{
    SubscriptBox["CsuMinus", 
     RowBox[{"HAC", " "}]], "[", 
    RowBox[{"NNN_", ",", " ", "alph_"}], "]"}], " ", 
   "\[IndentingNewLine]"}]}]], "Input",
 CellChangeTimes->{{3.46713134995374*^9, 3.46713138764695*^9}, {
  3.467131543826007*^9, 3.46713155334823*^9}, {3.467189008011303*^9, 
  3.467189010253325*^9}}]
},
WindowSize->{1600, 1128},
WindowMargins->{{3, Automatic}, {Automatic, 47}},
Magnification->1.5,
FrontEndVersion->"7.0 for Linux x86 (32-bit) (February 25, 2009)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[545, 20, 8343, 224, 917, "Input"],
Cell[8891, 246, 210, 5, 46, "Input"],
Cell[9104, 253, 737, 18, 197, "Input"]
}
]
*)

(* End of internal cache information *)
