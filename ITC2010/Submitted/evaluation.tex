\section{Evaluation}
\label{sec:evaluation}
In this section, we verify our analysis and evaluate the privacy and search efficiencies of the PANACEA system as related to unstructured and structured access-controlled P2P systems using simulation experiments.
\par
In our simulated system (implemented in Java), we assume $N=10000$ peers that use the system both to publish and search for resources. The PStores on the peers are initialized with $25$ random entries. The providers are organized in a Kademlia-like structured topology, but they are also connected over an unstructured overlay power-law network with average degree $7.5$ and maximum degree $150$. We conducted two types of simulation experiments, which differ in their resource distributions and the type of the generated queries. Each resource is randomly assigned a publisher peer and a list of user peers who are authorized to access the resource. Any other peer is said to be an unauthorized user for this resource and publisher pair. We compute $P_{V,a}$ and $P_{V,u}$ as follows: 
\begin{itemize}                                                             \item $P_{V,a}=1$ if a user is able to contact a publisher where he is authorized to access the requested resource.
\item If the resource key is found in the DHT with a value list of size $n'$, then $P_{V,a} = P_{V,u}= \frac{1}{n'}$.
\item Otherwise, if the resource key is not found in the DHT and no authorized copy is located by flooding, then $P_{V,a}= P_{V,u}=0$.
\end{itemize}
%$P_{K,u}$, $P_{V,u}$ are computed in the same way except that the unauthorized searches are never successful.
The PANACEA's parameters are taken as follows: key list size $m=5$, value list size $n=5$ and forwarding probability $\lambda=0.6$. Also, $ttl=4$ was employed for limited-hop flooding in the unstructured overlay.
\subsection{Provider privacy and search cost}
\label{prov_priv}
Initially, we aim to verify the correctness of eqs. (\ref{pva_panacea}), (\ref{csa_panacea}) using the simulation results with a rather static setting regarding resource popularity. Specifically, we assume $100$ resources with $N_c=50$ copies for each (thus $5K$ resources in total). $100$ peers are randomly selected, each of which is inserted into the authorization list of random $N_a=5$ copies. Each resource is then given to randomly chosen peers that publish them using the PANACEA publishing mechanism. The collision probability for provider lists is experimentally found to be $f_v=0.002$. We also experimentally found that by searching in the unstructured overlay $P_{V,a}^U=0.38$, $P_{V,u}=P_{-}=0$, $912$ distinct nodes are visited and $1687$ messages are sent per query on the average. In order to measure $P_{K,a},C_{s,a}$, we generate authorized searches from the above $100$ authorized peers for all of the $100$ resources, thus $10K$ search queries in total. Also, in order to measure $P_{K,u},C_{s,u}$, we randomly select $100$ unauthorized users that query the system for the same $100$ resources.
These experiments have been run $10$ times each and the mean values are plotted in Figures \ref{fig:pav_nanc} to \ref{fig:csu_nanc}. As depicted in Figures \ref{fig:pav_nanc}, \ref{fig:puv_nanc}, the analytical equations model the privacy properties of the simulated PANACEA system very accurately. As the probability of publishing $\mu$ increases, PANACEA approaches the search efficiency of a structured system (see Figure \ref{fig:pav_nanc}). Note that for only $N_a=5$ authorized copies in the system, a small value of $\mu=0.6$ makes the search efficiency of PANACEA close to that of structured systems. On the other hand, for unauthorized users, provider privacy of our system is always close to that of unstructured P2P systems, as shown in Figure \ref{fig:puv_nanc}.
Therefore, PANACEA design meets its privacy objectives of Section \ref{sec:analysis}. For $\mu=0$, $P_{V,u}=0$. When $\mu=0.1$, a provider list is found in the DHT for the queried resource, reducing the privacy in comparison to $\mu=0$. As $\mu$ increases, so is the size of provider list of the resource, and thus $P_{V,u}$ decreases. 
\begin{figure}
\begin{minipage}[b]{0.45\linewidth}
\centering
\includegraphics[height=4cm, width=4.5cm]{results/auth-nanc-V.pdf}
\caption{$P_{V,a}$ \textrm{vs} $\mu$}
\label{fig:pav_nanc}
\end{minipage}
\hspace{0.4cm}
\begin{minipage}[b]{0.45\linewidth}
\centering
\includegraphics[height=4cm, width=4.5cm]{results/unauth-nanc-V.pdf}
\caption{$P_{V,u}$ vs $\mu$}
\label{fig:puv_nanc}
\end{minipage}
\vspace{-0.6cm}
\end{figure}
\begin{figure}
\begin{minipage}[b]{0.45\linewidth}
\centering
\includegraphics[height=4cm, width=4.5cm]{results/auth-nanc-cost.pdf}
\caption{$C_{s,a}$ \textrm{vs} $\mu$}
\label{fig:csa_nanc}
\end{minipage}
\hspace{0.4cm}
\begin{minipage}[b]{0.45\linewidth}
\centering
\includegraphics[height=4cm, width=4.5cm]{results/unauth-nanc-cost.pdf}
\caption{$C_{s,u}$ vs $\mu$}
\label{fig:csu_nanc}
\end{minipage}
\vspace{-0.2cm}
\end{figure}
\begin{figure}
\begin{minipage}[b]{0.45\linewidth}
\centering
\includegraphics[height=4cm, width=4.5cm]{results/auth-gen-V.pdf}
\caption{$P_{V,a}$ \textrm{vs} $\mu$}
\label{fig:pva_gen}
\end{minipage}
\hspace{0.4cm}
\begin{minipage}[b]{0.45\linewidth}
\centering
\includegraphics[height=4cm, width=4.5cm]{results/unauth-gen-V.pdf}
\caption{$P_{V,u}$ vs $\mu$}
\label{fig:pvu_gen}
\end{minipage}
\vspace{-0.5cm}
\end{figure}
In Figure \ref{fig:csa_nanc}, the effect of $\mu$ on the search communication cost for authorized users is depicted. As $\mu$ increases, the probability to find a provider where the user is authorized also increases. After $\mu=0.6$, there is no more search cost improvement, because the size of the provider list of the queried resource slightly increases. As shown later, in general, the search cost significantly decreases as $\mu$ increases. However, as observed from Figure \ref{fig:csu_nanc}, the search cost for unauthorized users significantly increases (over the cost of limited-hop flooding) with $\mu$, which is a highly desirable property of PANACEA. 

Next, we evaluate the provider privacy and the search cost of PANACEA for authorized and unauthorized users in a more general setting, where $10K$ resources whose popularity ($N_c$) follows Zipf distribution are published in the system. The maximum number of resource copies is $150$ and their mean number is $10$ thus resulting in a total of $100K$ resources. A random number of $5$ to $50$ peers are chosen to be authorized to each resource. Each resource of the $100K$ ones is randomly assigned to a peer that initiates PANACEA publishing. We randomly generated $20K$ number of authorized and unauthorized search queries separately. Again, the experiments are repeated $10$ times and mean values of the results are plotted in Figure \ref{fig:pva_gen} to Figure \ref{fig:csu_gen}. As depicted in Figure \ref{fig:pva_gen}, search efficiency increases with $\mu$ for an authorized user. Also, as shown in Figure \ref{fig:pvu_gen}, provider privacy is always minimal for unauthorized users. Finally, Figure \ref{fig:csa_gen} depicts that the search cost for authorized users decreases with $\mu$, as opposed to that of unauthorized users that increases with $\mu$.
\begin{figure}
\begin{minipage}[b]{0.45\linewidth}
\centering
\includegraphics[height=4cm, width=4.5cm]{results/auth-gen-cost.pdf}
\caption{$C_{s,a}$ \textrm{vs} $\mu$}
\label{fig:csa_gen}
\end{minipage}
\hspace{0.4cm}
\begin{minipage}[b]{0.45\linewidth}
\centering
\includegraphics[height=4cm, width=4.5cm]{results/unauth-gen-cost.pdf}
\caption{$C_{s,u}$ vs $\mu$}
\label{fig:csu_gen}
\end{minipage}
\vspace{-0.2cm}
\end{figure} 
\begin{figure}
\begin{minipage}[b]{0.45\linewidth}
\centering
\includegraphics[height=4cm, width=4.3cm]{results/resource-entropy.pdf}
\caption{$H_{K}$ \textrm{vs} $c$}
\label{fig:r_entropy}
\end{minipage}
\hspace{0.4cm}
\begin{minipage}[b]{0.45\linewidth}
\centering
\includegraphics[height=4cm, width=4.3cm]{results/provider-entropy.pdf}
\caption{$H_{V}$ vs $c$}
\label{fig:p_entropy}
\end{minipage}
\vspace{-0.6cm}
\end{figure}
\subsection{Resource privacy}
\label{res_priv}
As mentioned in Section \ref{sec:approach}, resource privacy may be breached by observing large sizes of the provider lists. Although it is difficult to preserve resource privacy of highly popular resources, we do not focus on them as their presence in the system can be easily taken for granted. Our goal for resource privacy in PANACEA is to preserve privacy for the other resources. 
\par
We observe that as long as an existing resource has a provider list size less or equal to that of a phantom resource, an adversary cannot differentiate between them. We assume again $100K$ resources Zipf-distributed with mean $10$ and maximum $150$ copies. %Employing a small local static partition, of the resource namespace $R$ for resource anonymization, i.e. 
 For the $R_L$-approach with $|R_L|=25$, we observed that phantom keys have provider lists longer than those of the $87.6$ percentile of the existing resources in the DHT (for $\mu=1$). For this percentile of resources, the resource privacy for unauthorized users is $P_{K,u}=1/m=0.2$ and for the authorized users is $P_{K,a}=1$ for $\mu=1$. For more popular resources, the adversary can exploit the provider list sizes to conclude their existence. 

Finally, by numerically evaluating eqs. (\ref{pminus_panacea}) and (\ref{csminus_panacea}) with $|R|=2M$, $N_r=100K$, $\mu=1$, we observed the PANACEA system meets its design objectives in this case as well, as $P_{-} \sim P_{-}^{U}$ and $C_{s,-} \sim C_{s,-}^U$. We omit the verification of these formulas with simulations for brevity reasons.
\subsection{System entropy}
In Section \ref{sec:analysis}, we have defined the entropies for resource anonymity and provider anonymity. This formulation allows us to measure the privacy breach that can be achieved by collusive groups of size $c$ based on a simple modification to the formulas: The provider list of the $(m,n)$-entry of the queried resource will only contain $E(n')(1-c/N)$ non-collusive entries. Employing the parameters of Section \ref{res_priv} with $\mu=0.6$, $n_f=20$ and $|R|=2M$, we numerically evaluate the slightly modified versions of eqs. (\ref{r_entropy}) and (\ref{p_entropy}). As depicted in Figures \ref{fig:r_entropy}, \ref{fig:p_entropy}, the anonymity set by PANACEA for unauthorized users is $4.92\equiv m$ for resource privacy and the anonymity set for provider privacy is 36.7.