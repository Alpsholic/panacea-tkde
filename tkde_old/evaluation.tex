\section{Evaluation}
\label{sec:evaluation}
In this section, we evaluate the PANACEA system employing the FS-anonymization mentioned in Section \ref{sec:circular}. We present the results observed from simulation experiments and from a prototype deployment on PlanetLab described in detail separately in the following sections. The privacy and search metrics are studied as the system parameter $\mu$ is varied. In both the cases, we compute the parameters $P_{V,a}$ and $P_{V,u}$ as follows: 
\begin{itemize}
\item $P_{V,a}=1$ if a user is able to contact a publisher where he is authorized to access the
requested resource.
\item If the resource key is found in the DHT, then $P_{V,a} = P_{V,u}= \frac{1}{mn}$.
\item Otherwise, if the resource key is not found in the DHT and no authorized copy is located by
flooding, then $P_{V,a}= P_{V,u}=0$. 
\end{itemize}
The PANACEA's parameters are chosen as follows: key list size $m=4$, value list size $n=4$ and
forwarding probability $\lambda=0.6$. 
\subsection{Simulation based evaluation}
The main objective of the evaluation is to prove the correctness of the analytical modeling of the
PANACEA system's privacy and search efficiency presented in Section.
\ref{sec:analysis} for the single searcher and single query adversary model and
study the performance of the system in a more general setting. In addition, we quantitatively
position the PANACEA system w.r.t the state-of-the-art solution in the
literature for privacy preserving indexing of access controlled content
introduced in \cite{privacyPreservingIndex} discussed in Section \ref{sec:relatedwork}. This system is
referred as \textit{PPI} in the rest of the paper. The PPI approach deals with provider privacy only. It partitions the set of providers into several groups and resources published by a single provider are mapped to his/her entire groups so that the provider can not be distinguished from others. The static groups in PPI are analogous to dynamic anonymity provider set used in PANACEA. We considered the group size in the PPI case same as the value of $n$ in PANACEA. A qualitative comparison is presented in Section. \ref{sec:relatedwork}.
\par
We implemented the PANACEA and PPI simulators in Java. The PPI simulator implements the system presented in \cite{privacyPreservingIndex}. We assume $N=10000$ peers that use the system both to
publish and search for resources. The PStores on the peers are initialized with $25$ random entries.
The providers are organized in a Kademlia-like structured topology, but they are also connected over
an unstructured overlay power-law network with average degree $7.5$ and maximum degree $150$. We
conducted two types of simulation experiments, which differ in their resource distributions and the
type of the generated queries. Each resource considered, is randomly assigned a publisher peer and a list of user
peers who are authorized to access the resource. Any other peer is said to be an unauthorized user for
this resource and publisher pair. Also, $ttl=4$ was employed for limited-hop flooding in the
unstructured overlay.
\par
We run a total of $3$ different experiments on the simulator and measure the resource, provider privacies including the search cost in all experiments described in the following.
%\subsubsection{Results}
\subsubsection{Privacy and search cost}
\underline {Experiment -1:} Initially, we aim to verify the correctness of eqs. (\ref{pva_panacea}), (\ref{csa_panacea}) using the
simulation results with a rather static setting regarding resource popularity. Specifically, we assume
$100$ resources with $N_c=50$ copies for each (thus $5K$ resources in total). $100$ peers are randomly
selected, each of which is inserted into the authorization list of random $N_a=5$ copies. Each
resource is then given to randomly chosen peers that publish them using the PANACEA publishing
mechanism. The collision probability for provider lists is experimentally found to be $f_v=0.002$. We also experimentally found that by searching in the unstructured overlay $P_{V,a}^U=0.38$, $P_{V,u}=P_{-}=0$, a total of
$912$ distinct nodes are visited and $1687$ messages are sent per query on the average. In order to
measure $P_{K,a}$ and $C_{s,a}$, we generate authorized searches from the above $100$ authorized peers for
all of the $100$ resources, thus $10K$ search queries in total. Also, in order to measure
$P_{K,u}$ and $C_{s,u}$, we randomly select $100$ unauthorized users that query the system for the same
$100$ resources.
These experiments have been run $10$ times each and the mean values are plotted in Figures
\ref{fig:kv_nanc}, \ref{fig:cost_nanc}\footnote{The privacy properties of PPI analytically are presented in Appendix and are skipped from the plots for brevity. The analytical model plots overlap with that of simulations.}. As depicted in these figures, the analytical equations model the privacy properties of the simulated PANACEA system very accurately, thus verifying the analysis presented in Section \ref{sec:single}. As the probability of publishing $\mu$ increases, PANACEA approaches the search efficiency of a structured system (see Figures \ref{fig:pak_nanc}, \ref{fig:pav_nanc}). Note that for only $N_a=5$
authorized copies in the system, a small value of $\mu=0.6$ makes the search efficiency of PANACEA
close to that of structured systems. The PPI approach as mentioned already, does not protect resource privacy and thus $P_{K,a}=P_{K,u}=1$ as shown in Figures \ref{fig:pak_nanc}, \ref{fig:puk_nanc}. For authorized users, PANACEA shows performance similar to PPI after $\mu=0.6$ but significantly improves resource privacy for unauthorized users (maximum $P_{K,u}$ is only $0.25$, as show in Figure \ref{fig:puk_nanc}, compared to $1$ achieved by PPI). Thanks to provider's existence conditioned on resource existence, PANACEA enhances provider privacy over PPI as shown in Figure \ref{fig:puv_nanc} where PPI offers a flat privacy of $0.25$ and and PANACEA closer to that of unstructured systems. Therefore, PANACEA design meets its privacy objectives introduced in Section \ref{sec:analysis}. For $\mu=0$, $P_{V,u}=0$. From $\mu=0.1$ onwards, a provider list is found in the DHT for the queried resource, resulting in a constant privacy value of $\frac{1}{mn(1-f_v)}$. 
\par
In Figure \ref{fig:csa_nanc}, the effect of $\mu$ on the search communication cost for authorized
users is depicted. As $\mu$ increases, the probability to find a provider where the user is authorized
also increases. After $\mu=0.6$, there is no more search cost improvement, because the size of the
provider list of the queried resource slightly increases. As shown later, in general, the search cost
significantly decreases as $\mu$ increases. However, as observed from Figure \ref{fig:csu_nanc}, the
search cost for unauthorized users significantly increases (over the cost of limited-hop flooding)
with $\mu$, which is a highly desirable property of PANACEA. PPI shows a flat search cost equal to the selectivity of the resource times the group size \footnote{as shown in the analytical formulation in the Appendix}.
\begin{figure}[htb]%
\centering
\subfloat[][$P_{K,a}$]{\includegraphics[height=3.9cm,
width=4.3cm]{results/auth-nanc-K.pdf}
\label{fig:pak_nanc}}%
\subfloat[][$P_{K,u}$]{\includegraphics[height=3.9cm,
width=4.3cm]{results/unauth-nanc-K.pdf}
\label{fig:puk_nanc}}%
\qquad
\subfloat[][$P_{V,a}$]{\includegraphics[height=3.9cm,
width=4.3cm]{results/auth-nanc-V.pdf}
\label{fig:pav_nanc}}%
\subfloat[][$P_{V,u}$]{\includegraphics[height=3.9cm,
width=4.3cm]{results/unauth-nanc-V.pdf}
\label{fig:puv_nanc}}%
\caption{Privacy performance (Experiment- 1)}%
%%\vspace*{-0.6cm}
\label{fig:kv_nanc}%
\end{figure}
\begin{figure}[htb]%
\centering
\subfloat[][$C_{s,a}$]{\includegraphics[height=3.9cm,
width=4.3cm]{results/auth-nanc-cost.pdf}
\label{fig:csa_nanc}}%
\subfloat[][$C_{s,u}$]{\includegraphics[height=3.9cm,
width=4.3cm]{results/unauth-nanc-cost.pdf}
\label{fig:csu_nanc}}%

\caption{Search performance (Experiment- 1)}%
%%\vspace*{-0.6cm}
\label{fig:cost_nanc}%
\end{figure}
\par
\underline {Experiment -2:} Next, we evaluate the privacy and the search performance of PANACEA for authorized and unauthorized users in a more general setting, where $10K$ resources whose popularity ($N_c$) follows Zipf
distribution are published in the system. The maximum number of resource copies is $150$ and their
mean number is $10$ thus resulting in a total of $100K$ resources. A random number of $5$ to $50$
peers are chosen to be authorized to each resource. Each resource of the $100K$ ones is randomly
assigned to a peer that initiates PANACEA publishing. This case represents the plots labeled $PANACEA\_random$ emphasizing the uniform random distribution of the parameter $N_a$. In \underline{Experiment -3}, the value of $N_a$ is a fixed fraction of $N_c$ unlike the case of $PANACEA\_random$ which chooses $N_a$ randomly. We study the system for two different cases where $N_a$ is $10\%$ and $20\%$ of $N_c$ (represented by $PANACEA\_0.10$ and $PANACEA\_0.20$ respectively in the plots).
\par
In both cases, we randomly generated $20K$ number of authorized and unauthorized search queries separately. Again, the experiments are repeated $10$ times and mean values of the results are plotted in Figures \ref{fig:kv_nabync}, \ref{fig:cost_nabync}. 
\par
For $PANACEA\_random$, as depicted in Figures \ref{fig:pak_nabync}, \ref{fig:pav_nabync}, the search efficiency increases with $\mu$ for an authorized user reaching maximum at $\mu=1$. For $PANACEA\_0.10$ and $PANACEA\_0.20$, the $P_{K,a}$ and $P_{V,a}$ show performance similar to that of Experiment-1 (Figures \ref{fig:pak_nanc}, \ref{fig:pav_nanc}). As $\frac{N_a}{N_c}$ increases, PANACEA's performance moves closer to that of the ideal case of $P_{K,a}=P_{V,a}=1$. Note that the performance shown is average of $20K$ queries chosen randomly not considering their $N_a$ and $N_c$ values unlike in the case of Experiment -1. However, all the cases ensure a minimal resource and provider privacy for unauthorized users as shown in Figures \ref{fig:puk_nabync}, \ref{fig:puv_nabync}. Also, Figure \ref{fig:csa_nabync} depicts that the search cost for authorized users decreases with $\mu$, as opposed to that of unauthorized users as shown in Figure \ref{fig:csu_nabync}.
\begin{figure}[htb]%
\centering
\subfloat[][$P_{K,a}$]{\includegraphics[height=3.9cm,
width=4.3cm]{results/auth-gen-and-naBync-K.pdf}
\label{fig:pak_nabync}}%
\subfloat[][$P_{K,u}$]{\includegraphics[height=3.9cm,
width=4.3cm]{results/unauth-gen-and-naBync-K.pdf}
\label{fig:puk_nabync}}%
\qquad
\subfloat[][$P_{V,a}$]{\includegraphics[height=3.9cm,
width=4.3cm]{results/auth-gen-and-naBync-V.pdf}
\label{fig:pav_nabync}}%
\subfloat[][$P_{V,u}$]{\includegraphics[height=3.9cm,
width=4.3cm]{results/unauth-gen-and-naBync-V.pdf}
\label{fig:puv_nabync}}%
\caption{Privacy performance (Experiment- 2\&3)}%
%%\vspace*{-0.6cm}
\label{fig:kv_nabync}%
\end{figure}

\begin{figure}[htb]%
\centering
\subfloat[][$C_{s,a}$]{\includegraphics[height=3.9cm,
width=4.3cm]{results/auth-gen-and-naBync-cost.pdf}
\label{fig:csa_nabync}}%
\subfloat[][$C_{s,u}$]{\includegraphics[height=3.9cm,
width=4.3cm]{results/unauth-gen-and-naBync-cost.pdf}
\label{fig:csu_nabync}}%
\caption{Search performance (Experiment- 2\&3)}%
%%\vspace*{-0.6cm}
\label{fig:cost_nabync}%
\end{figure}
\subsubsection{System entropy}
\label{entropy}
We demonstrate the entropy analysis discussed in Section \ref{sec:entropy} in Figure \ref{fig:entropy} with parameters $N_a=5, N_c=10$. For $\mu=0$, the entropy $H_{K,u}\approx 18$ which is roughly equivalent to an anonymity set of size $\approx 200K$($=\frac{\mid R \mid}{N_c}$). As $\mu$ increases, the entropy decreases due to the resource presence in the DHT. For authorized user, entropy gradually reduces with $\mu$ and becomes zero after $\mu=0.6$. The provider entropy is demonstrated in Figure \ref{fig:p_entropy}. $H_{V,u}\approx 10$ (=$N_c/N$) for $\mu=0$. After $\mu=0.6$, the provider entropy becomes zero for authorized user. At $\mu=1$, for an unauthorized user the anonymity set size would be $n=4$, which is verified by the plot as the entropy is $2$ 
as shown in Figure \ref{fig:p_entropy}.

\begin{figure}[htb]%
\centering
\subfloat[][$H_{K}$]{\includegraphics[height=3.9cm,
width=4.3cm]{results/resource-entropy.pdf}
\label{fig:r_entropy}}%
\subfloat[][$H_{V}$]{\includegraphics[height=3.9cm,
width=4.3cm]{results/provider-entropy.pdf}
\label{fig:p_entropy}}%
\caption{Entropy performance}%
%%\vspace*{-0.6cm}
\label{fig:entropy}%
\end{figure}
\subsection{PlanetLab based evaluation}
\textcolor{red}{TODO}
\subsection{Discussion}
Out of different types of experiments shown above, the experiment- 3 makes a more realistic choice as a resource becomes more popular (thus $N_c$ increases), one can expect a reasonable proportionate increase in number of providers the user is authorized with ($N_a$). The results shown for this case are promising and prove that PANACEA is a viable solution to meet both conflicting goals of privacy preservation and search optimization. However, as seen in the results, the performance for the authorized user can be improved further (and hence $P_{K,a}=P_{V,a}=1$, by choosing a value for $\mu$ independently for different resources in the system, which is in accordance with its popularity ($N_c$). For example, the search efficiency can be increased by choosing high $\mu$ for unpopular resources. We envision that the publisher can approximately
anticipate the popularity value of a resource based on either type of resource or past search
experiences and choose appropriate value for $\mu$.
\par
In addition, in systems where privacy of non-existence of resources is not mandatory, the value of $\mu$ can be
set to $1$ without breach of any privacy. %In other systems, a higher $\mu$ value is preferred for less popular resources in order to improve the efficiency for authorized users and a lower value is considerable for highly popular resources. 

% 
% \begin{figure}
% \begin{minipage}[b]{0.45\linewidth}
% \centering
% \includegraphics[height=4cm, width=4cm]{results/auth-nanc-K.pdf}
% \caption{$P_{K,a}$ \textrm{vs} $\mu$}
% \label{fig:pak_nanc}
% \end{minipage}
% \hspace{0.4cm}
% \begin{minipage}[b]{0.45\linewidth}
% \centering
% \includegraphics[height=4cm, width=4cm]{results/unauth-nanc-K.pdf}
% \caption{$P_{K,u}$ vs $\mu$}
% \label{fig:puk_nanc}
% \end{minipage}
% \end{figure}
% \begin{figure}
% \begin{minipage}[b]{0.45\linewidth}
% \centering
% \includegraphics[height=4cm, width=4cm]{results/auth-nanc-V.pdf}
% \caption{$P_{V,a}$ \textrm{vs} $\mu$}
% \label{fig:pav_nanc}
% \end{minipage}
% \hspace{0.4cm}
% \begin{minipage}[b]{0.45\linewidth}
% \centering
% \includegraphics[height=4cm, width=4cm]{results/unauth-nanc-V.pdf}
% \caption{$P_{V,u}$ vs $\mu$}
% \label{fig:puv_nanc}
% \end{minipage}
% \end{figure}
% \begin{figure}
% \begin{minipage}[b]{0.45\linewidth}
% \centering
% \includegraphics[height=4cm, width=4cm]{results/auth-nanc-cost.pdf}
% \caption{$C_{s,a}$ \textrm{vs} $\mu$}
% \label{fig:csa_nanc}
% \end{minipage}
% \hspace{0.4cm}
% \begin{minipage}[b]{0.45\linewidth}
% \centering
% \includegraphics[height=4cm, width=4cm]{results/unauth-nanc-cost.pdf}
% \caption{$C_{s,u}$ vs $\mu$}
% \label{fig:csu_nanc}
% \end{minipage}
% \end{figure}

% \begin{figure}
% \begin{minipage}[b]{0.45\linewidth}
% \centering
% \includegraphics[height=4cm, width=4cm]{results/auth-gen-K.pdf}
% \caption{$P_{K,a}$ \textrm{vs} $\mu$}
% \label{fig:pav_nanc}
% \end{minipage}
% \hspace{0.4cm}
% \begin{minipage}[b]{0.45\linewidth}
% \centering
% \includegraphics[height=4cm, width=4cm]{results/unauth-gen-K.pdf}
% \caption{$P_{K,u}$ vs $\mu$}
% \label{fig:puv_nanc}
% \end{minipage}
% \end{figure}
% 
% \begin{figure}
% \begin{minipage}[b]{0.45\linewidth}
% \centering
% \includegraphics[height=4cm, width=4cm]{results/auth-gen-V.pdf}
% \caption{$P_{V,a}$ \textrm{vs} $\mu$}
% \label{fig:pav_nanc}
% \end{minipage}
% \hspace{0.4cm}
% \begin{minipage}[b]{0.45\linewidth}
% \centering
% \includegraphics[height=4cm, width=4cm]{results/unauth-gen-V.pdf}
% \caption{$P_{V,u}$ vs $\mu$}
% \label{fig:puv_nanc}
% \end{minipage}
% \end{figure}
% 
% \begin{figure}
% \begin{minipage}[b]{0.45\linewidth}
% \centering
% \includegraphics[height=4cm, width=4cm]{results/auth-gen-cost.pdf}
% \caption{$C_{s,a}$ \textrm{vs} $\mu$}
% \label{fig:csa_nanc}
% \end{minipage}
% \hspace{0.4cm}
% \begin{minipage}[b]{0.45\linewidth}
% \centering
% \includegraphics[height=4cm, width=4cm]{results/unauth-gen-cost.pdf}
% \caption{$C_{s,u}$ vs $\mu$}
% \label{fig:csu_nanc}
% \end{minipage}
% \end{figure}




% \begin{figure}
% \begin{minipage}[b]{0.45\linewidth}
% \centering
% \includegraphics[height=4cm, width=4cm]{results/auth-gen-and-naBync-K.pdf}
% \caption{$P_{K,a}$ \textrm{vs} $\mu$ }
% \label{fig:pav_nabync}
% \end{minipage}
% \hspace{0.4cm}
% \begin{minipage}[b]{0.45\linewidth}
% \centering
% \includegraphics[height=4cm, width=4cm]{results/unauth-gen-and-naBync-K.pdf}
% \caption{$P_{K,u}$ vs $\mu$ }
% \label{fig:puv_nabync}
% \end{minipage}
% \end{figure}
% 
% \begin{figure}
% \begin{minipage}[b]{0.45\linewidth}
% \centering
% \includegraphics[height=4cm, width=4cm]{results/auth-gen-and-naBync-V.pdf}
% \caption{$P_{V,a}$ \textrm{vs} $\mu$ }
% \label{fig:pav_nabync}
% \end{minipage}
% \hspace{0.4cm}
% \begin{minipage}[b]{0.45\linewidth}
% \centering
% \includegraphics[height=4cm, width=4cm]{results/unauth-gen-and-naBync-V.pdf}
% \caption{$P_{V,u}$ vs $\mu$ }
% \label{fig:puv_nabync}
% \end{minipage}
% \end{figure}
% 
% \begin{figure}
% \begin{minipage}[b]{0.45\linewidth}
% \centering
% \includegraphics[height=4cm, width=4cm]{results/auth-gen-and-naBync-cost.pdf}
% \caption{$C_{s,a}$ \textrm{vs} $\mu$ }
% \label{fig:csa_nabync}
% \end{minipage}
% \hspace{0.4cm}
% \begin{minipage}[b]{0.45\linewidth}
% \centering
% \includegraphics[height=4cm, width=4cm]{results/unauth-gen-and-naBync-cost.pdf}
% \caption{$C_{s,u}$ vs $\mu$ }
% \label{fig:csu_nabync}
% \end{minipage}
% \end{figure}



% \begin{figure}
% \begin{minipage}[b]{0.45\linewidth}
% \centering
% \includegraphics[height=4cm, width=4.3cm]{results/resource-entropy.pdf}
% \caption{$H_{K}$ \textrm{vs} $\mu$}
% \label{fig:r_entropy}
% \end{minipage}
% \hspace{0.4cm}
% \begin{minipage}[b]{0.45\linewidth}
% \centering
% \includegraphics[height=4cm, width=4.3cm]{results/provider-entropy.pdf}
% \caption{$H_{V}$ vs $\mu$}
% \label{fig:p_entropy}
% \end{minipage}
% \end{figure}






% \subsubsection{FS approach vs RS approach}
% \begin{itemize}
%  \item \textcolor{red}{Put 1/2 plots..already 18pages-discuss esp resource privacy- and our findings on popularity thing}
% \item \textcolor{red}{Show distribution of genuine resources and fake resources- in FS, both overlap (or same pattern but scaled) and in RS, not. IMP for understanding the paper.}
% \end{itemize}